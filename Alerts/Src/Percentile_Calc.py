#EH=['2017-01-03 16:00:00','2017-01-12 11:00:00','2017-01-24 12:00:00']
#peak_data = concatenate(app_name,percentage)

def percentile_cal(peak_data,EH,pct=[80,95],ema_win_len=3,high=0.75,med=0.2,low=0.05):
    tot=DataFrame()
    tks=DataFrame()
    TI=EH[:]
    TI.append('Total')
    tot=DataFrame()
    kks=DataFrame()
    tks=DataFrame()
    if(len(peak_data.filter('Txn_').columns)>0):
        res=DataFrame(index=['Total'],columns=['{}_{}'.format(a, b) for b in pct for a in ['Kpis','Txns','Total']])
    else:
        res=DataFrame(index=['Total'],columns=['{}_{}'.format(a, b) for b in pct for a in ['Kpis','Total']])
    for tt in EH:
        frm=tt
        to=tt
        ema_data=ema(peak_data,frm,to,ema_win_len)
        Result = apply_weightage(ema_data,high,med,low)
        Txns=Result.filter(regex='Txn')
        Kpis=Result[list(set(Result.columns)-set(Txns.columns))]
        tot=tot.append(DataFrame({'Values':Result.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        if(len(peak_data.filter('Txn_').columns)>0):
            tks=tks.append(DataFrame({'Txns':Txns.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        kks=kks.append(DataFrame({'Kpis':Kpis.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        for pp in pct:
            res['Kpis_'+str(pp)][tt]=np.percentile(Kpis.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values,pp)
            if(len(peak_data.filter('Txn_').columns)>0):
                res['Txns_'+str(pp)][tt]=np.percentile(Txns.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values,pp)
            res['Total_'+str(pp)][tt]=np.percentile(Result.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values,pp)
    for pp in pct:
        res['Kpis_'+str(pp)]['Total']=np.percentile(kks,pp)
        if(len(peak_data.filter('Txn_').columns)>0):
            res['Txns_'+str(pp)]['Total']=np.percentile(tks,pp)
        res['Total_'+str(pp)]['Total']=np.percentile(tot,pp)
    res.to_csv('Percentile_EH.csv')



# Non Event Hours:
    
#peak_data=peak_data[~peak_data.Time.isin(['2017-01-03 14:00:00','2017-01-03 15:00:00','2017-01-03 16:00:00','2017-01-12 09:00:00','2017-01-12 10:00:00','2017-01-12 11:00:00','2017-01-24 10:00:00','2017-01-24 11:00:00','2017-01-24 12:00:00'])]
#peak_data=peak_data.reindex(range(0,len(peak_data)))
#frm=peak_data.Time[0]
#to=str(peak_data.Time[len(peak_data)-1])
#tot=DataFrame()
#kks=DataFrame()
#tks=DataFrame()
#if(len(peak_data.filter('Txn_').columns)>0):
#    res=DataFrame(index=['Total'],columns=['{}_{}'.format(a, b) for b in pct for a in ['Kpis','Txns','Total']])
#else:
#    res=DataFrame(index=['Total'],columns=['{}_{}'.format(a, b) for b in pct for a in ['Kpis','Total']])
#ema_data=ema(peak_data,frm,to,ema_win_len)
#Result = apply_weightage(ema_data,high,med,low)
#Txns=Result.filter(regex='Txn')
#Kpis=Result[list(set(Result.columns)-set(Txns.columns))]
#tot=tot.append(DataFrame({'Values':Result.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
#tks=tks.append(DataFrame({'Txns':Txns.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
#kks=kks.append(DataFrame({'Kpis':Kpis.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
#for pp in pct:
#    res['Kpis_'+str(pp)]['Total']=np.percentile(kks,pp)
#    if(len(peak_data.filter('Txn_').columns)>0):
#        res['Txns_'+str(pp)]['Total']=np.percentile(tks,pp)
#    res['Total_'+str(pp)]['Total']=np.percentile(tot,pp)
#res.to_csv(app_name+'_Percentile_NEH.csv')


def percentile_cal_layer_wise(peak_data,EH,comp_info,pct=[80,95],ema_win_len=3,high=0.75,med=0.2,low=0.05):
    tot=DataFrame()
    tks=DataFrame()
    TI=EH[:]
    TI.append('Total')
    lay=list(comp_info[comp_info.Layer!='TXN'].Layer.unique())
    layers=list(comp_info[comp_info.Layer!='TXN'].Layer.unique())
    if('TXN' in layers):
        lay.append('Txns')
    lay.append('Total')
    #res=DataFrame(index=TI,columns=['{}_{}'.format(a, b) for b in pct for a in ['Kpis','Txns','Total']])
    res=DataFrame(index=TI,columns=['{}_{}'.format(a, b) for b in pct for a in lay])    
    total_vals={}
    for tt in EH:
        frm=tt
        to=tt
        ema_data=ema(peak_data,frm,to,ema_win_len)
        Result = apply_weightage(ema_data,high,med,low)
        Txns=Result.filter(regex='Txn')
        Kpis=Result[list(set(Result.columns)-set(Txns.columns))]
        dict_of_df = {}        
        for ll in layers:
            comp_name=comp_info[comp_info.Layer==ll].Name.values
            if(len(comp_name)>1):
                cols=[]
                for c in comp_name:
                    cols.append(list(Kpis.filter(regex=c).columns))
                cols=[j for i in cols for j in i]
            else:
                cols=Kpis.filter(regex=comp_name[0]).columns
            dict_of_df[ll]=Kpis[cols]
            if(tt==EH[0]):
                total_vals[ll]=DataFrame()
            total_vals[ll]=total_vals[ll].append(DataFrame({'Kpis':Kpis[cols].replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        tot=tot.append(DataFrame({'Values':Result.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        tks=tks.append(DataFrame({'Txns':Txns.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        #kks=kks.append(DataFrame({'Kpis':Kpis.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values}))
        for pp in pct:
            for ll in layers:
                vals=dict_of_df[ll].replace(0,np.nan).mean(axis=1,skipna=True).dropna().values
                if(len(vals)>0):
                    res[ll+'_'+str(pp)][tt]=np.percentile(vals,pp)
                else:
                    res[ll+'_'+str(pp)][tt]=0
            if('TXN' in layers):
                res['Txns_'+str(pp)][tt]=np.percentile(Txns.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values,pp)
            res['Total_'+str(pp)][tt]=np.percentile(Result.replace(0,np.nan).mean(axis=1,skipna=True).dropna().values,pp)
    for pp in pct:
        for ll in layers:
            vals=total_vals[ll]
            if(len(vals)>0):              
                res[ll+'_'+str(pp)]['Total']=np.percentile(vals,pp)
            else:
                res[ll+'_'+str(pp)]['Total']=0
        if('TXN' in layers):
            res['Txns_'+str(pp)]['Total']=np.percentile(tks,pp)
        res['Total_'+str(pp)]['Total']=np.percentile(tot,pp)
    res.to_csv('Percentile_EH.csv')