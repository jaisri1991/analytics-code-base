def anomalydef_scoring(bl,comp_info,app_name,method='PC',kpi_threshold=.25,txn_threshold=.25,pcl=95,percentage=.5,roll_apply=60,ema_win_len=3,high=0.75,med=0.2,low=0.05):
    ano_data=df_anomalydef(bl,comp_info,app_name,percentage,roll_apply)
    peak_data = concatenate(app_name,percentage)
    frm=peak_data.Time[0]
    to=peak_data.Time[len(peak_data)-1]
    ema_data=ema(peak_data,frm,to,ema_win_len)
    Result = apply_weightage(ema_data,high,med,low)
    Result=Result.fillna(0)
    
    if(method=='Natural'):
        Kpis,Txns=natural(Result)
    elif(method=='PC'):
        Kpis,Txns=perc_comp_wise(Result,pcl,kpi_threshold,txn_threshold)
    elif(method=='PK'):
        Kpis,Txns=perc_kpi_wise(Result,pcl)
        
    if(len(Txns.columns)>0):
        Txns['Time']=Txns.index
    else:
        Txns=DataFrame({'Time':Result.index})
    if(len(Kpis.columns)>0):
        Kpis['Time']=Kpis.index    
        Res=Kpis.merge(Txns,on='Time',how='outer')    
        cols = [col for col in Res.columns if col not in ['Time']]
        ano_data['Anomaly_Scored']=0
        ano_data.index=range(0,len(ano_data))
        for kpi in cols:
            comp_id=bl[bl.Metric_Name==kpi].CompInstanceId.values[0]
            comp_name=comp_info[comp_info.CompId==comp_id].Name.values[0]
            if(kpi[0:4] != 'Txn_'):
                met=kpi.replace(comp_name+'_','')
            else:
                met=kpi
            ind=ano_data[ano_data.Metric_Name==met].index
            ano_data['Anomaly_Scored'][ind]=Res[kpi] 
        return ano_data
    else:
        print('No Data')
        
        
#def anomalydef_scoring(bl,comp_info,app_name,layer_threshold,txn_threshold=.25,pcl=95,percentage=.5,roll_apply=60,ema_win_len=3,high=0.75,med=0.2,low=0.05):
#    ano_data=df_anomalydef(bl,comp_info,app_name,percentage,roll_apply)
#    peak_data = concatenate(app_name,percentage)
#    frm=peak_data.Time[0]
#    to=peak_data.Time[len(peak_data)-1]
#    ema_data=ema(peak_data,frm,to,ema_win_len)
#    Result = apply_weightage(ema_data,high,med,low)
#    Txns=Result.filter(regex='Txn')
#    lay=comp_info[comp_info.Layer!='TXN'].Layer.unique()
#    Kpis=Result[list(set(Result.columns)-set(Txns.columns))]   
#    dict_of_df = {}        
#    for ll in lay:
#        comp_name=comp_info[comp_info.Layer==ll].Name.values
#        if(len(comp_name)>1):
#            cols=[]
#            for c in comp_name:
#                cols.append(list(Kpis.filter(regex=c).columns))
#            cols=[j for i in cols for j in i]
#        else:
#            cols=Kpis.filter(regex=comp_name[0]).columns
#        dict_of_df[ll]=Kpis[cols]
#    for rr in Result.index:        
#        for ll in lay:
#            if(np.percentile(dict_of_df[ll].loc[rr],pcl) < layer_threshold[layer_threshold.Layer==ll].Values.values[0]):
#                Kpis.loc[rr,dict_of_df[ll].columns]=0
#        if(len(Txns.columns)>0):
#            if(np.percentile(Txns.loc[rr],pcl) < txn_threshold):
#                Txns.loc[rr]=0
#    if(len(Txns.columns)>0):
#        Txns['Time']=Txns.index
#    else:
#        Txns=DataFrame({'Time':Result.index})
#    if(len(Kpis.columns)>0):
#        Kpis['Time']=Kpis.index    
#        Res=Kpis.merge(Txns,on='Time',how='outer')    
#        cols = [col for col in Res.columns if col not in ['Time']]
#        ano_data['Anomaly_Scored']=0
#        ano_data.index=range(0,len(ano_data))
#        for kpi in cols:
#            comp_id=bl[bl.Metric_Name==kpi].CompInstanceId.values[0]
#            comp_name=comp_info[comp_info.CompId==comp_id].Name.values[0]
#            if(kpi[0:4] != 'Txn_'):
#                met=kpi.replace(comp_name+'_','')
#            else:
#                met=kpi
#            ind=ano_data[ano_data.Metric_Name==met].index
#            ano_data['Anomaly_Scored'][ind]=Res[kpi] 
#        return ano_data
#    else:
#        print('No Data')