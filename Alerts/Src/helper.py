#@profile(stream=fp)
def fetch_data(hostip,app_id,frm,to,comp_list,txn_list,txn_kpis='Total,AvgRespTime,SlowPercentage'):
    t0=time.time()
    t1 = time.time()
    dquery = ('select G.Id CompId,G.AliasName Name,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C where G.CompMetricSetId=C.Id and G.Id in (%s);' %comp_list)
    data_refresh = DataFrame()
    com_data=DataFrame()
    try:
        if(len(comp_list)>0):
            com_data = db_result(dquery)
            com_data['Layer']=len(com_data) * ['NA']
        else:
                print('No Comp Id Given')
        try:
            if(len(txn_list)>0):
                tquery = ('select distinct TxnId CompId,Name from TRANSACTIONCOLL where AppInstanceId=%s and Time between \'%s\' and \'%s\' and TxnId in (%s) order by Name'%(app_id,frm,to,txn_list))
                tco_data = db_result(tquery)
                if(len(tco_data)>0):
                    tco_data['Colltab']='TRANSACTIONCOLL'
                    tco_data['Layer']='TXN'
                    com_data=com_data.append(tco_data,ignore_index=True)
                    com_data['App_Id'] = app_id
                    print('Started Collecting Transaction Data')
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip, frm, to,app_id,'Total,AvgRespTime,SlowPercentage',txn_list)) as url:
                    #   txn_data = json.loads(url.read().decode())
                    r = requests.get("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip, frm, to,app_id,txn_kpis,txn_list))
                    txn_data=r.json()
                    if ((txn_data['responseStatus'] in ('Sucess', 'success')) & (isinstance(txn_data['result'], list)) & (len(txn_data['result'])>1)):
                        txn_data = DataFrame(txn_data['result'])
                        #txn_data = txn_data[:-1]
                        txn_data['Time'] = pd.to_datetime(txn_data['Time'], format='%Y-%m-%d %H:%M:%S')
                        #txn_data['Time']=pd.to_datetime((txn_data['Time']+ datetime.timedelta(hours=5, minutes=30)).apply(lambda x: x.strftime('%Y-%m-%d %H:%M:%S')),format='%Y-%m-%d %H:%M:%S')
                        print('Transaction Data Set %s,%s'%(txn_data.shape[0],txn_data.shape[1]))
                        data_refresh = txn_data
                        print('Completed Transaction Data')
                    else:
                        print('No Txn Data For the selected Date range')
                else:
                    print('No Txn Data For the selected Date range')
            else:
                print('No Txn Id Given')
            Layer = com_data
            if (len(comp_list) > 0):
                for i in com_data[(com_data.Layer !='TXN')].CompId:
                    colltab = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Colltab.item()
                    comp_name = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item()
                    print('Started Collecting Data For %s' % comp_name)
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab)) as url:
                    #    kpidata = json.loads(url.read().decode())
                    r=requests.get("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab))
                    kpidata=r.json()
                    if ((kpidata['responseStatus'] in ('Sucess', 'success')) & (isinstance(kpidata['result'], list)) & (len(kpidata['result'])>1)):
                        data = DataFrame(kpidata['result'])
                        #data = data[:-1]
                        print('%s Data Set %s,%s' % (comp_name,data.shape[0], data.shape[1]))
                        data['Time'] = pd.to_datetime(data['Time'], format='%Y-%m-%d %H:%M:%S')
                        Layer.set_value(Layer[(Layer.CompId == i)&(Layer.Layer !='TXN')].CompId.index.item(), 'Layer',
                                        re.sub(' ', '_', kpidata['properties']['layerName']))
                        col=['Time']
                        col.extend([col for col in data.columns if
                               col not in ['MININGCLUSTERID', 'BMININGCLUSTERID', 'TRANSACTION_CLUSTER_ID',
                                           'COMPINSTANCEID','Time']])
                        #col.extend(['Time'])
                        data = data[col]
                        cols=['Time']
                        cols.extend([com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item() + '_' + str(j) for j in data.iloc[:, data.columns != 'Time'].columns])
                        #cols.extend(['Time'])
                        data.columns=cols
                        #if ((i == com_data.CompId[0]) & (txn_list=='NULL')):
                        if(len(data_refresh)==0):
                            data_refresh = data_refresh.append(data)
                        else:
                            data_refresh = pd.merge(data_refresh, data, on='Time', how='outer',sort=True)
                        print('Collected Data For %s' % comp_name)
                    else:
                        print('No data for the selected date range')
            if(len(data_refresh)>0):
                print("Time Taken To Fetch Data: %s" % (time.time() - t1))
                t1=time.time()
                #com_data=pd.merge(com_data,Layer,how='outer',on=
                com_data=Layer
                #print("Filling Missing Values")
                #rng = pd.date_range(start=min(data_refresh.Time), periods=data_refresh.shape[0], freq=str(1) + 'min')
                #rng = pd.DataFrame(rng, columns=['Time'])
                cols = [col for col in data_refresh.columns if col not in ['Time']]
                data_refresh['Time'] = pd.DatetimeIndex(data_refresh['Time'])
                data_refresh[cols] = data_refresh[cols].apply(pd.to_numeric, errors='coerce')
                #complete_data = pd.merge(data_refresh, rng, how='outer', on=['Time'])
                #complete_data=do(complete_data,cols,5)
                #print("Time Taken To Fill Missing Values: %s" % (time.time() - t1))
        except:
            print('Check if the DataService is running')
    except:
        print('DB Connection Failed')
    print("Total Time Taken To Fetch KPI Data From DB: %s" % (time.time() - t0))
    return data_refresh,com_data

def plot_java(app_name,percentage,win_len):

    files=os.listdir('Data/'+app_name+'/output/')
    ano_details=pd.DataFrame({'KPI':[0]*len(files),'Arima_Mean':0,'Nrow':0})
    j=0
    #df=non_overlapping_average(dd,win_len,'mean')
    for file in files:
        kpi=file.replace('.csv','')
        data=pd.read_csv('Data/'+app_name+'/output/'+file)
        data.columns=data.columns.str.strip()
        data.rename(columns={'3SD Anomalies':kpi}, inplace=True)
        c = [col for col in data.columns if col not in ['Time']]
        data[c]=data[c].apply(pd.to_numeric, errors='coerce')
        if not os.path.exists('Plots/Java/'+app_name):
            os.makedirs('Plots/Java/'+app_name)
        os.chdir('Plots/Java/'+app_name)
        data.index=pd.to_datetime(data.Time,format='%d-%m-%Y %H:%M')
        axvl=data[round(len(data)*percentage):].index[0]
        plt.figure(figsize=(12,10))
        x = data['Actual']
        #y1 = data['Lower Interval']
        #y2 = data['Upper Interval']
        #y = data['Forecasts']
        plt.plot(x,color='orange')
        #plt.fill_between(x.index,y2,y1,interpolate=True,facecolor='green',alpha=0.2)
        #plt.plot(y,'--',color='b')
        high=data[(data[kpi]>0)].index
        plt.plot(data['Actual'][high],'ro',color='r')
        plt.axvline(x=axvl,color='r')
        plt.xticks(rotation=45)
        plt.title(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc.jpg')
        ano_details.loc[j,'KPI']=kpi
        ano_details.loc[j,'Arima_Mean']=len(high)
        ano_details.loc[j,'Nrow']=len(data[(data['Forecasts']>0)])
        j=j+1
        os.chdir('../../..')
    ano_details.to_csv('ano_details.csv')


def db_result(query):
    conn=dbfun()
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        df=pd.DataFrame(rows)
        if(len(df)>0):
            df.columns=[i[0] for i in cursor.description]
            print('Total Row(s):', cursor.rowcount)
        return df
    except Exception as e:
        print(e)
    finally:
        cursor.close()
def for_stl(data_set,app_name):
    cols = [col for col in data_set.columns if col not in ['Time']]
    if not os.path.exists('Data/'+app_name+'/Indi_Kpis/'):
            os.makedirs('Data/'+app_name+'/Indi_Kpis/')
    os.chdir('Data/'+app_name+'/Indi_Kpis/')
    for c in cols:
        df=DataFrame({'Time':data_set.Time,c:data_set[c]})
        df=df[['Time',c]]
        df.to_csv(c+'.csv',index=False)
    os.chdir('../../../')
    

def stl_data(app_name):
    #os.chdir('java')
    files=os.listdir(Base_Dir+'/Data/'+app_name+'/STLOutput')
#    files=list(set(files) - set([name for name in files if 'KPI_' in name]))
#    files=list(set(files) - set([name for name in files if 'STL_' in name]))
#    files.sort()
    for file in files:
        kpi=file.replace('.csv','')
        #os.system('java -jar spark-arima-1.0-SNAPSHOT.jar -d ..\Data\\'+app_name+'\Indi_Kpis\\'+file+' -m S')
        sdata=pd.read_csv(Base_Dir+'/Data/'+app_name+'/STLOutput'+'/'+file)
        sdata.Time = pd.to_datetime(sdata.Time,format='%d-%m-%Y %H:%M')
        if(file==files[0]):
            stl=sdata
        else:
            stl=stl.merge(sdata,on='Time',how='outer',sort=True)
    os.chdir('../')
    stl.to_csv(Base_Dir+'/Data/'+app_name+'/STL_Preprocessed.csv',index=False)
    
    
def comp_txn(app_name):
    app_list=pd.read_csv(Base_Dir+'/Data/app_list.csv')
    app=app_list[app_list.App_Name==app_name]
    comp_list=app.Comp_List.values[0]
    txn_list=app.Txn_List.values[0]
    app_id=app.App_Id.values[0]
    return app_id,comp_list,txn_list

def non_overlapping_average(raw_df,window_len,aggr_method='mean'):
    averaged_df=DataFrame()
    if((isinstance(raw_df.index,pd.core.indexes.datetimes.DatetimeIndex))):
        if (aggr_method == 'mean'):
            averaged_df = raw_df.resample(str(window_len)+'Min').mean()
        elif (aggr_method == 'median'):
            averaged_df = raw_df.resample(str(window_len)+'Min').median()
        elif (aggr_method == 'skew'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.skew)
        elif (aggr_method == 'kurt'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.kurt)
    else:
        print('DataFrame Index is not DateTime')
    return averaged_df

def impute_using_sma(x,cols,n):
    #t1 = time.time()
    #print('Imputation Process Started')
    if(isinstance(cols,list)):
        for f in cols:
          #print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    #print("Total Time Taken For Imputation Process: %s" % (time.time() - t1))
    return x

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isnull(x.ix[0,f]) == True):
        nn = x[f].notnull().nonzero()[0]
        if (len(nn) > 1):
            x.ix[0, f]=x.ix[nn[0]:nn[1]+1, f].mean()
        else:
            x.ix[0, f] =0
    if (count > n):
        ### Method is SMA
        row_no = pd.isnull(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if((len(row_no)==count-1)&(x.ix[0, f]==0)):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()
                    
def plot_birch(kpi_imputated_data,result,app_name,win_len,percentage):
    cols=list(result.Kpi.unique())    
    for kpi in cols:
        act_data=kpi_imputated_data[['Time',kpi]]
        act_data.index=act_data.Time
        act_data.drop('Time',axis=1,inplace=True)
        data=result[result.Kpi==kpi]
        if not os.path.exists('Plots/Birch/'+app_name):
            os.makedirs('Plots/Birch/'+app_name)
        os.chdir('Plots/Birch/'+app_name)
        data.index=pd.to_datetime(data.Time)
        axvl=data.index[0]
        plt.figure(figsize=(12,10))
        x = data['Actual_Data']
        plt.plot(act_data,color='orange')
        high=data[(data['Anomaly']>0)].index
        plt.plot(x[high],'ro',color='r')
        plt.axvline(x=axvl,color='r')
        plt.xticks(rotation=45)
        plt.title(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc.jpg')
        os.chdir('../../..')