def perc_kpi_wise(Result,pcl):
    if(len(Result)>0):
        for k in Result.columns:
            dd=Result[[k]]
            thres=np.percentile(dd,pcl)
            dd[dd < thres]=0
            Result[k]=dd
        Txns=Result.filter(regex='Txn')
        Kpis=Result[list(set(Result.columns)-set(Txns.columns))]
        return Kpis,Txns

def natural(Result):
    if(len(Result)>0):        
        norm_df=(Result-Result.mean())/Result.std()
        norm_df=norm_df.fillna(0)
        Txns=norm_df.filter(regex='Txn')
        Kpis=norm_df[list(set(norm_df.columns)-set(Txns.columns))] 
        Kpis[Kpis < 3] = 0
        Txns[Txns < 3] = 0
        return Kpis,Txns
    
def perc_comp_wise(Result,pcl,kpi_threshold,txn_threshold):
    if(len(Result)>0):
        Txns=Result.filter(regex='Txn')
        Kpis=Result[list(set(Result.columns)-set(Txns.columns))]            
        for rr in Result.index:
            if(len(Kpis.columns)>0):
                if(np.percentile(Kpis.loc[rr],pcl) < kpi_threshold):
                    Kpis.loc[rr]=0
            if(len(Txns.columns)>0):
                if(np.percentile(Txns.loc[rr],pcl) < txn_threshold):
                    Txns.loc[rr]=0
        Kpis[Kpis < kpi_threshold] =0
        Txns[Txns < txn_threshold] =0
        return Kpis,Txns