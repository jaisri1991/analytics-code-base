def main(path,frm,to,sub_kpi,percentage=0.5,win_len=3,high=0.75,med=0.2,low=0.05,fast=False):
    Date1=frm
    Date2=to
    peak_data = concatenate(path,percentage)
#    try:
#        frm_index = peak_data[peak_data.Time==frm].index.values[0]
#    except:
#        frm=peak_data.Time[0]
#        frm_index = peak_data[peak_data.Time == frm].index.values[0]
#    try:
#        to_index = peak_data[peak_data.Time == to].index.values[0]
#    except:
#        to=peak_data.Time[len(peak_data)-1]
#        to_index = peak_data[peak_data.Time == to].index.values[0]
#    dd=peak_data[:]
#    if(frm_index > win_len):
#        frm = peak_data.Time[frm_index-(win_len-1)]
#        dd = peak_data[(peak_data.Time >= frm) & (peak_data.Time <= to)]
#    if(to_index < len(dd)):
#        dd = peak_data[(peak_data.Time >= frm) & (peak_data.Time <= to)]
#    cols = [col for col in dd.columns if col not in ['Time']]
#    ema_data = dd[cols].ewm(span = win_len,adjust = False).mean()
#    ema_data.index = dd.Time
    ema_data=ema(peak_data,frm,to,win_len)
    Result = apply_weightage(ema_data,high,med,low)
    #Result = Result[Result.index >= Date1]
    Result=Result[Result.sum()[Result.sum() > 0].index]
    Corr_Kpis=DataFrame()

    if(len(Result) > 0):
        if(fast==True):
            Corr=pd.DataFrame(index=Result.columns,columns=Result.columns)
            for i in Result.columns:
                for j in Result.columns:
                    distance,pa=fastdtw(Result[i],Result[j],dist=euclidean)
                    Corr[i][j]=distance
        else:
            Corr = Result.corr()
        if(fast==True):
             Corr.to_csv('Results/Detailed/Corr_'+Date1[0:13].replace('-','_')+'th_Hour_to_'+Date2[0:13].replace('-','_')+'th_Hour_fastdtw.csv',index=False)          
        else:
             Corr.to_csv('Results/Detailed/Corr_'+Date1[0:13].replace('-','_')+'th_Hour_to_'+Date2[0:13].replace('-','_')+'th_Hour.csv',index=False)
        if((sub_kpi=='All')|(sub_kpi=='all')):
            for kpi in Result.columns:
                if(fast==True):
                    h = Corr[kpi].sort_values(ascending=True).drop(labels=kpi)
                else:
                    h = Corr[kpi].sort_values(ascending=False).drop(labels=kpi)
                h.dropna(inplace=True)
                if (len(h) > 0):
                    txn_kpis=list(filter(lambda x:'Txn' in x, h.index))
                    kpi_l=[x for x in h.index if x not in txn_kpis]
                    c = DataFrame({'Subjected_KPI': kpi, 'Correlated_KPIs': ','.join(kpi_l),'Correlated_Txns': ','.join(txn_kpis),'Correlation_KPI_Values': ','.join(['%.3f' % num for num in h[kpi_l].values]),'Correlation_Txn_Values': ','.join(['%.3f' % num for num in h[txn_kpis].values])}, index=[0])
                    if((kpi==Result.columns[0]) | (len(Corr_Kpis)==0)):
                        Corr_Kpis=c
                    else:
                        Corr_Kpis=Corr_Kpis.append(c)
                else:
                    print('None of the KPIs Correlated For ' + kpi)
        else:
            if(fast==True):
                h = Corr[sub_kpi].sort_values(ascending=True).drop(labels=sub_kpi)
            else:
                h = Corr[sub_kpi].sort_values(ascending=False).drop(labels=sub_kpi)            
            h.dropna(inplace=True)
            if (len(h) > 0):
                txn_kpis=list(filter(lambda x:'Txn' in x, h.index))
                kpi_l=[x for x in h.index if x not in txn_kpis]
                Corr_Kpis = DataFrame({'Subjected_KPI': sub_kpi, 'Correlated_KPIs': ','.join(kpi_l),'Correlated_Txns': ','.join(txn_kpis),'Correlation_KPI_Values': ','.join(['%.3f' % num for num in h[kpi_l].values]),'Correlation_Txn_Values': ','.join(['%.3f' % num for num in h[txn_kpis].values])}, index=[0])
                
                #Corr_Kpis = DataFrame({'Subjected_KPI': sub_kpi, 'Correlated_KPIs': ','.join(h.index),'Correlation_Values': ','.join(['%.3f' % num for num in h.values])}, index=[0])
            else:
                print('None of the KPIs Correlated For ' + sub_kpi)
    elif((sub_kpi!='All')&(sub_kpi!='all')):
        if(fast==True):
            h=Result.iloc[0].sort_values(ascending=True).drop(sub_kpi)
        else:
            h=Result.iloc[0].sort_values(ascending=False).drop(sub_kpi)           
        h=h.loc[(h!=0)]
        if (len(h) > 0):
            txn_kpis=list(filter(lambda x:'Txn' in x, h.index))
            kpi_l=[x for x in h.index if x not in txn_kpis]
            Corr_Kpis = DataFrame({'Subjected_KPI': sub_kpi, 'Correlated_KPIs': ','.join(kpi_l),'Correlated_Txns': ','.join(txn_kpis),'Correlation_KPI_Values': ','.join(['%.3f' % num for num in h[kpi_l].values]),'Correlation_Txn_Values': ','.join(['%.3f' % num for num in h[txn_kpis].values])}, index=[0])
            
            #Corr_Kpis = DataFrame({'Subjected_KPI': sub_kpi, 'Correlated_KPIs': ','.join(h.index),'Correlation_Values': ','.join(['%.3f' % num for num in h.values])}, index=[0])
        else:
            print('None of the KPIs Correlated For ' + sub_kpi)
    else:
        print('Please Change the Subjected KPI From All/all to Something Else')
    if(len(Corr_Kpis)>0):
        Corr_Kpis = Corr_Kpis[['Subjected_KPI', 'Correlated_KPIs','Correlated_Txns', 'Correlation_KPI_Values','Correlation_Txn_Values']]
        Corr_Kpis.index = range(0, len(Corr_Kpis))
    if(fast==True):
        Corr_Kpis.to_csv('Results/Summary/'+Date1[0:13].replace('-','_')+'th_Hour_to_'+Date2[0:13].replace('-','_')+'th_Hour_Fastdtw.csv',index=False)
    else:
        Corr_Kpis.to_csv('Results/Summary/'+Date1[0:13].replace('-','_')+'th_Hour_to_'+Date2[0:13].replace('-','_')+'th_Hour.csv',index=False)
    return Corr_Kpis