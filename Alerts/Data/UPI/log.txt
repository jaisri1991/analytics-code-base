
G:\Python\analytics-code-base\Alerts\java>java -jar spark-arima-1.0-SNAPSHOT.jar -m F -n 0.5 -p ..\D
ata\UPI\Parameters.csv -sd 0.006,0.003 -d ..\..\..\Data_Set\UPI\Raw_df_From_Aug_To_Nov_2017_only_DB_
and_APP.csv
Length of Training data ::: 1464
############################
KPI ::: UPI_APP_Cluster_COMPLETED_REQUEST_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/G:/Python/analytics-code-base/Alerts/java/lib/slf4j-log4j12-1.7.2
2.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/G:/Python/analytics-code-base/Alerts/java/lib/slf4j-simple-1.6.1.
jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.slf4j.impl.Log4jLoggerFactory]

ARIMA Coefficients ==========
autoregressive: 0.7353
moving-average: 0.2732
mean: -37805.7716
intercept: -10007.2433

Total time taken for UPI_APP_Cluster_COMPLETED_REQUEST_COUNT is === 13002 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_EXECUTE_THREAD_IDLE_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.7459
moving-average: 0.0877
mean: 0.0930
intercept: 0.0236

Total time taken for UPI_APP_Cluster_EXECUTE_THREAD_IDLE_COUNT is === 13161 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_EXECUTE_THREAD_TOTAL_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.6859
moving-average: 0.1959
mean: 0.2261
intercept: 0.0710

Total time taken for UPI_APP_Cluster_EXECUTE_THREAD_TOTAL_COUNT is === 12772 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_HEAP_FREE_CURRENT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5951
moving-average: 0.1076
mean: 300345.1593
intercept: 121611.8775

Total time taken for UPI_APP_Cluster_HEAP_FREE_CURRENT is === 15764 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_HEAP_FREE_PERCENT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5176
moving-average: 0.2200
mean: 0.0006
intercept: 0.0003

Total time taken for UPI_APP_Cluster_HEAP_FREE_PERCENT is === 14881 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_HEAP_SIZE_CURRENT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.7046
moving-average: -0.0456
mean: 68829.5957
intercept: 20335.0653

Total time taken for UPI_APP_Cluster_HEAP_SIZE_CURRENT is === 10910 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_HOGGING_THREAD_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.3284
moving-average: 0.3244
mean: 0.0064
intercept: 0.0043

Total time taken for UPI_APP_Cluster_HOGGING_THREAD_COUNT is === 11976 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_OPEN_SOCKETS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.4139
moving-average: 0.2435
mean: 0.0105
intercept: 0.0062

Total time taken for UPI_APP_Cluster_OPEN_SOCKETS is === 11702 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_PENDING_USER_REQUEST_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: -0.0898
moving-average: 0.3359
mean: -0.0005
intercept: -0.0006

Total time taken for UPI_APP_Cluster_PENDING_USER_REQUEST_COUNT is === 11407 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_STANDBY_THREAD_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5650
moving-average: 0.2496
mean: -0.0564
intercept: -0.0245

Total time taken for UPI_APP_Cluster_STANDBY_THREAD_COUNT is === 13187 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_THREAD_POOL_QUEUE_LENGTH
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: -0.0902
moving-average: 0.3362
mean: -0.0005
intercept: -0.0006

Total time taken for UPI_APP_Cluster_THREAD_POOL_QUEUE_LENGTH is === 11609 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_THROUGHPUT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.3227
moving-average: 0.3093
mean: -0.0630
intercept: -0.0427

Total time taken for UPI_APP_Cluster_THROUGHPUT is === 11585 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_TRANSACTIONS_ACTIVE_TOTAL_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 1.0230
moving-average: 0.3733
zero mean

Total time taken for UPI_APP_Cluster_TRANSACTIONS_ACTIVE_TOTAL_COUNT is === 21065 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_TRANSACTION_COMMITTED_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 1.0230
moving-average: 0.3733
zero mean

Total time taken for UPI_APP_Cluster_TRANSACTION_COMMITTED_COUNT is === 20167 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_TRANSACTION_ROLLEDBACK_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 1.0230
moving-average: 0.3733
zero mean

Total time taken for UPI_APP_Cluster_TRANSACTION_ROLLEDBACK_COUNT is === 20921 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_APP_Cluster_TRANSACTION_TOTAL_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 1.0230
moving-average: 0.3733
zero mean

Total time taken for UPI_APP_Cluster_TRANSACTION_TOTAL_COUNT is === 19856 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_BYTES_RECEIVED
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5858
moving-average: 0.2716
mean: -272.3496
intercept: -112.8099

Total time taken for UPI_DB_Cluster_BYTES_RECEIVED is === 11273 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_BYTES_SENT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5446
moving-average: 0.2555
mean: -1284.0635
intercept: -584.7727

Total time taken for UPI_DB_Cluster_BYTES_SENT is === 12116 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_CACHE_HITRATIO
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.7308
moving-average: 0.4384
mean: -0.0000
intercept: -0.0000

Total time taken for UPI_DB_Cluster_CACHE_HITRATIO is === 13324 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_CPU_USAGE
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: -0.0132
moving-average: 0.4954
mean: 1.2858
intercept: 1.3028

Total time taken for UPI_DB_Cluster_CPU_USAGE is === 12419 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_CURRENT_LOGONS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.2765
moving-average: 0.6765
mean: -0.0876
intercept: -0.0634

Total time taken for UPI_DB_Cluster_CURRENT_LOGONS is === 10735 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_CURRENT_OPEN_CURSORS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.2940
moving-average: 0.6765
mean: -0.3709
intercept: -0.2619

Total time taken for UPI_DB_Cluster_CURRENT_OPEN_CURSORS is === 10563 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_EXECUTE_COUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5682
moving-average: 0.2319
mean: -1.6161
intercept: -0.6978

Total time taken for UPI_DB_Cluster_EXECUTE_COUNT is === 11092 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_HARD_PARSECOUNT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.0813
moving-average: 0.5747
mean: 0.0247
intercept: 0.0227

Total time taken for UPI_DB_Cluster_HARD_PARSECOUNT is === 13735 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_LOCKS_WAITS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.4206
moving-average: 0.1803
mean: 0.0039
intercept: 0.0023

Total time taken for UPI_DB_Cluster_LOCKS_WAITS is === 11001 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_LOGICAL_READS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.6937
moving-average: 0.0956
mean: 3629.7173
intercept: 1111.8153

Total time taken for UPI_DB_Cluster_LOGICAL_READS is === 11969 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_NUMBEROF_SESSIONS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.1518
moving-average: 0.3780
mean: 0.0149
intercept: 0.0126

Total time taken for UPI_DB_Cluster_NUMBEROF_SESSIONS is === 11296 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_PHYSICAL_READS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.3955
moving-average: 0.0466
mean: 1062.1865
intercept: 642.1372

Total time taken for UPI_DB_Cluster_PHYSICAL_READS is === 11520 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_PHYSICAL_WRITES
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.6611
moving-average: -0.2422
mean: 8.9152
intercept: 3.0217

Total time taken for UPI_DB_Cluster_PHYSICAL_WRITES is === 14472 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_SORTS_DISK
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5184
moving-average: -0.1010
mean: 0.0000
intercept: 0.0000

Total time taken for UPI_DB_Cluster_SORTS_DISK is === 27323 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_SORTS_ROWS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5036
moving-average: -0.0850
mean: 44.1382
intercept: 21.9114

Total time taken for UPI_DB_Cluster_SORTS_ROWS is === 11405 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_TABLESCANS_LONG
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.1328
moving-average: -0.0296
mean: 0.0001
intercept: 0.0001

Total time taken for UPI_DB_Cluster_TABLESCANS_LONG is === 22377 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_TABLESCANS_SHORT
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5271
moving-average: 0.1778
mean: -0.0746
intercept: -0.0353

Total time taken for UPI_DB_Cluster_TABLESCANS_SHORT is === 11722 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_USER_CALLS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.5983
moving-average: 0.2696
mean: -4.4373
intercept: -1.7824

Total time taken for UPI_DB_Cluster_USER_CALLS is === 12250 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_USER_COMMITS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.6158
moving-average: 0.2777
mean: 0.6837
intercept: 0.2626

Total time taken for UPI_DB_Cluster_USER_COMMITS is === 11700 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
############################
KPI ::: UPI_DB_Cluster_USER_ROLLBACKS
P = 1, D = 0, Q = 1
Size of hourly data : 2928
Size of Training data : 1464
Size of Test data : 1464
********************* LOOP THROUGH THE TEST DATA *************

ARIMA Coefficients ==========
autoregressive: 0.4048
moving-average: 0.4096
mean: -0.0002
intercept: -0.0001

Total time taken for UPI_DB_Cluster_USER_ROLLBACKS is === 11629 ms
Forecasts length : 1465
Upper interval length : 1465
Lower interval length : 1465
Anomaly Classifier length : 1464
 ==================================
Exception in thread "main" java.lang.NoClassDefFoundError: com/univocity/parsers/common/processor/Ro
wProcessor
        at personal.spark.arima.DataProcessor.modelAndForecast(DataProcessor.java:414)
        at personal.spark.arima.Main.runModelAndForecast(Main.java:153)
        at personal.spark.arima.Main.main(Main.java:39)
Caused by: java.lang.ClassNotFoundException: com.univocity.parsers.common.processor.RowProcessor
        at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
        at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:335)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
        ... 3 more

G:\Python\analytics-code-base\Alerts\java>