# Loading Libraries
import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
import statsmodels.api as sm
import statistics as st
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn import svm
import schedule
import time
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

import statsmodels.api as sm
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

import mysql.connector as mysqlcon
from pandas import DataFrame
from sqlalchemy import create_engine
import time
import datetime
import gc
import re
#from memory_profiler import profile
import urllib.request, json
import pickle
#from memory_profiler import LogFile
import plotly.tools as tls
import plotly.plotly as py
import requests
from flask import Flask
import urllib
from flask import render_template
from flask import request
from urllib.parse import urlparse
from flask import jsonify
import json
from flask import Response
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
#import maxentropy
#from vectorizedsampleentropy import vectsampen as vse
#from vectorizedsampleentropy import vectapen as vae
from scipy import stats
from sklearn2pmml import PMMLPipeline
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
from sklearn.cluster import Birch