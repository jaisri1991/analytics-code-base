# os.chdir('../../')
import os
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '\\Src'
exec(open(Base_Dir+'\\Include\\include.py').read())


#app_name='CoreBanking'
#hostip='192.168.2.166'
#app_id,comp_list,txn_list=comp_txn(app_name)
#frm='2017-01-01'
#to='2017-01-31'
#data_set,comp_info=fetch_data(hostip,app_id,frm,to,comp_list,'')
#ind=DataFrame({'Time':pd.date_range(min(data_set.Time),max(data_set.Time),freq='1min')})
#data_set=ind.merge(data_set,on='Time',how='outer',sort=True)
#data_set=data_set.fillna(method='ffill')
#data_set=data_set.fillna(method='bfill')
#data_set=data_set.fillna(0)
#data_set.to_csv('Data/'+app_name+'/Raw_df_From_July_To_Nov_2017.csv',index=False)
#comp_info.to_csv(Base_Dir+'/Data/'+app_name+'/Comp_Info.csv',index=False)

#stl_data(app_name)

bl=pd.read_csv(Base_Dir+'/Data/'+app_name+'/Parameters.csv')
comp_info=pd.read_csv(Base_Dir+'/Data/'+app_name+'/Comp_Info.csv')
method='PC'
pcl=95
pct=[10,30,50,80,90,95,100]
percentage=0.5
roll_apply=60
ema_win_len=3
txn_threshold=.25
kpi_threshold=.25
high=0.75
med=0.25
low=None
#Gives the result dataframe
ano_data=anomalydef_scoring(bl,comp_info,app_name,method,kpi_threshold,txn_threshold,pcl,percentage,roll_apply,ema_win_len,high,med)
ano_data.to_csv(Base_Dir+'/Data/'+app_name+'/ano_data.csv',index=False,header=False)

frm='2017-01-03 15:00:00'
to='2017-01-03 15:00:00'
#sub_kpi='UPI_DB_ALL_Locks_Waits'
#Calls Correlation function
Result=main(path,frm,to,'All',percentage,ema_win_len,high,med,low,False)



#Birch Function
app_name='CoreBanking'
hostip='192.168.2.166'
frm='2016-12-01'
to='2017-01-31'
roll_apply=60
ema_win_len=3
nc=16
percentage=0.5
stl=True
txn_imputated_data,kpi_imputated_data=pre_process(app_name,hostip,frm,to,roll_apply,ema_win_len)
txn_stl,kpi_stl=birch_stl(txn_imputated_data,kpi_imputated_data)
if(stl==True):
    result=birch_main(txn_stl,kpi_stl,nc,percentage)
    result.to_csv('Birch_Result_with_stl.csv')    
else:
    result=birch_main(txn_imputated_data,kpi_imputated_data,nc,percentage)
    result.to_csv('Birch_Result_without_stl.csv')
plot_birch(kpi_imputated_data,result,app_name,roll_apply,percentage)