def feature_wrapper(df,SeqL=False,SeasL=False,TF=False,BH=False,WW=False,DA=False,DHA=False,lag=24,length=168,label='1_Week',hour=('09:00','18:00')):
    df2=DataFrame({'Time':df.index},index=df.index)
    data=[]
    if(SeqL==True):
        seq_lag = features.Sequential_Lag(df2,lag)
        data.append(seq_lag)
    if(SeasL==True):
        seas_lag = features.Seasonal_Lag(df2,length,label)
        data.append(seas_lag)
    if(TF == True):
        tf = features.Time_Features(df2)
        data.append(tf)
    if(BH == True):
        bh = features.Business_Hour(df2,hour)
        data.append(bh)
    if(WW == True):
        wday_wend = features.Weekday_or_WeekEnd(df2)
        data.append(wday_wend)
    if (DA == True):
        daily_avg = features.Daily_Average(df2)
        data.append(daily_avg)
    if (DHA == True):
        daily_hour_avg = features.Daily_Hourly_Average(df2)
        data.append(daily_hour_avg)
    df2['Time'] = df2.index
    for edf in data:
        df2 = df2.merge(edf, on='Time', how='outer')
    df2 = df2.dropna()
    df['Time']=df.index
    df2=df2.merge(df,on='Time', how='outer')
    df2.index = df2.Time
    df2.drop('Time', axis=1, inplace=True)
    return df2