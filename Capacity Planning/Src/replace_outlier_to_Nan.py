def replace_outlier_to_Nan(df,peak_data):
    re=DataFrame()
    if(len(df)==len(peak_data)): #& (len(df.columns)==len(peak_data.columns))):
        cols = [col for col in df.columns if col not in ['Time']]
        for co in cols: 
            ind=peak_data[(peak_data[co]>0)].index
            if(len(ind)>0):
                df[co][ind]=np.NaN
        re=df[:]
    else:
        print('DataFrame rows and columns doesnt match')
    return re