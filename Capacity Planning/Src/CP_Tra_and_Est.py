class CP_Pre_Transformer(TransformerMixin):
    
    def __init__(self,win_len=60,aggr_method='mean',lag=24,hour=('09:00','18:00'),length=168,label='1_Week',kpi_name='CPU_UTIL'):
        self.win_len=win_len
        self.aggr_method=aggr_method
        self.lag=lag
        self.hour=hour
        self.length=length
        self.label=label
        self.kpi_name=kpi_name
        
    def transform(self,df):
        print('Window Length:'+str(self.win_len))
        hour_data=non_overlapping_average(df,self.win_len,self.aggr_method)
        hour_data['Time']=hour_data.index
        df2=DataFrame({'Time':hour_data.index},index=hour_data.index)
        tf=features.Time_Features(df2)
        bh=features.Business_Hour(df2,self.hour)
        wday_wend=features.Weekday_or_WeekEnd(df2)
        #seq_lag=features.Sequential_Lag(hour_data.filter(regex=self.kpi_name),self.lag)
        #seas_lag=features.Seasonal_Lag(hour_data.filter(regex=self.kpi_name),self.length,self.label)
        #seas_lag=seas_lag[seas_lag.columns.drop(list(seas_lag.filter(regex=self.kpi_name)))]
        #seq_lag=seq_lag[seq_lag.columns.drop(list(seq_lag.filter(regex=self.kpi_name)))]
        
        for edf in [tf,bh,wday_wend]:
            df2=df2.merge(edf,on='Time',how='outer')
        df2=df2.dropna()
        result=hour_data.merge(df2,on='Time',how='outer',right_index=True)
        result.drop('Time', axis=1, inplace=True)        
        return result
    
    def fit(self, *_):
        return self
    
class CP_Post_Transformer(TransformerMixin):
    
    def __init__(self,win_len=60,aggr_method='mean',hour=('09:00','18:00'),kpi_name='CPU_UTIL'):
        self.win_len=win_len
        self.aggr_method=aggr_method
        self.hour=hour
        self.kpi_name=kpi_name
        
    def transform(self,df):
        print('Window Length:'+str(self.win_len))
        df2=DataFrame({'Time':df.Time},index=df.index)        
        tf=features.Time_Features(df2)
        bh=features.Business_Hour(df2,self.hour)
        wday_wend=features.Weekday_or_WeekEnd(df2)       
        for edf in [tf,bh,wday_wend]:
            df2=df2.merge(edf,on='Time',how='outer')
        df2=df2[df2.columns.drop(list(df2.filter(regex=self.kpi_name)))]
        result=df2
        result.drop('Time', axis=1, inplace=True)        
        return result
    
    def fit(self, *_):
        return self
    

#class CP_Linear_reg_Estimator(TransformerMixin):
#    
#    def __init__(self,cols,Classification=True,percentage=0.8,nhead=1,aggr_method='mean',elapsed=0,win_len=60):
#        self.cols=cols
#        self.Classification=Classification
#        self.percentage=percentage
#        self.nhead=nhead
#        self.aggr_method=aggr_method
#        self.win_len=win_len
#        self.elapsed=elapsed
#   
#    def transform(self,result):
#        print('Linear Regression Estimator for '+str(self.win_len))        
#        output=Linear_reg(result,self.cols,self.Classification,self.percentage,self.nhead,self.elapsed,self.win_len)
#        return output
#    
#    def fit(self, X, y=None):
#        return self

#X_Test=X_Train.mean(axis=0)
#X_Test.iloc[0:70]=X_Train.filter(regex='Txn|shift|Same').mean(axis=0)*1.2
#cp_post_pp=Pipeline([('cp_post_tr',CP_Post_Transformer(win_len=win_len,aggr_method=aggr_method,hour=('09:00','18:00')))])
#df=DataFrame({'CPU_UTIL':range(0,1),'Time':pd.to_datetime('2017-11-12 12:17:00')})
#df.index=df.Time
#X_Test[70:]=np.concatenate([np.array(i) for i in np.array(cp_post_pp.transform(df))])
#
#
#esti_pp.fit(X_Train,Y_Train)
#importances = regr_rf.feature_importances_
#Feat_importance=DataFrame({'Features':X_Train.columns,'Importances':importances})
#Feat_importance=Feat_importance.sort_values('Importances',ascending=False)
#Feat_importance.head()