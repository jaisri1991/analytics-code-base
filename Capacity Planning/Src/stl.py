def stl(data):
    df=data.fillna(0)
    cols = [col for col in df.columns if col not in ['Time']]
    for co in cols:  
        try:
            r=seasonal_decompose(df[co],model='multiplicative')
        except:
            r=seasonal_decompose(df[co],model='addictive')
        if(len(r.resid)>0):
            dd=DataFrame({'Time':df.index,co:r.resid})
            dd=dd.fillna(0)
            if(co==cols[0]):
                result=dd
            else:
                result=pd.merge(result,dd,on='Time',how='outer')