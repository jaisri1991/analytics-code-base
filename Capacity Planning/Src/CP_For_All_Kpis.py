def accuracy_for_all(imputate_data,method='LR',percentage=0.9,time_feature=True,for_loop=False,nhead=5):
    txn_cols=[col for col in imputate_data if col.startswith('Txn_')]
    txn_cols.append('Time')
    kpi_cols=imputate_data.columns.difference(txn_cols)
    
    if(len(kpi_cols)>0):
        if(time_feature==True):
            #cp_pre_pp=Pipeline([('cp_pre_tr',CP_Pre_Transformer(win_len=win_len,aggr_method=aggr_method,lag=lag,hour=('09:00','18:00'),length=length,label='1_Week',kpi_name='hi'))])
            #full_result=cp_pre_pp.transform(imputate_data)
            full_result=feature_wrapper(imputate_data,SeqL=False,SeasL=False,TF=True,BH=True,WW=True,DA=False,DHA=False)
    
        Result_df=DataFrame({'KPI':kpi_cols,'MSE':['NA']*len(kpi_cols),'R2':['NA']*len(kpi_cols)})
        j=-1
        
        for kpi in kpi_cols:
            j=j+1
            if(time_feature==True):
                result=full_result[list(full_result.columns.difference(kpi_cols))+[kpi]] 
            else:
                result=imputate_data[txn_cols[:-1] + [kpi]]  
            count=round(len(result)*percentage)      
            cols = [col for col in result.columns if col not in [kpi]]
            X_Train=result[cols][:count]
            Y_Train=result[kpi][:count]
            X_Test=result[cols][count:]
            Y_Test=result[kpi][count:]
            if(method=='LR'):
                regr = linear_model.LinearRegression()
            if(method=='RF'):
                regr = RandomForestRegressor(max_depth=30, random_state=2)
            esti_pp=Pipeline([('method',regr)])
            final=DataFrame()
            try:
                if(for_loop==False):
                    print(kpi)
                    esti_pp.fit(X_Train,Y_Train)
                    final=DataFrame({kpi:esti_pp.predict(X_Test)},index=X_Test.index)
                else:
                    print(kpi)
                    for i in range((len(X_Test) / nhead).__round__()+1):            
                        if (i == 0):
                            x_Train = X_Train
                            x_Test = X_Test[0:nhead]
                            y_Train = Y_Train
                            #Y_Test = Test[0:5].Actual.reshape(-1,1)
                        else:
                            x_Train=result[cols][(i*nhead):(len(X_Train)+(i*nhead))]
                            x_Test=X_Test[i*nhead:(i+1)*nhead]
                            y_Train=result[kpi][(i*nhead):(len(X_Train)+(i*nhead))]
                        if(len(x_Test)>0):
                            esti_pp.fit(x_Train,y_Train)
                            Y_Pred=DataFrame({kpi:esti_pp.predict(x_Test)},index=x_Test.index)
                            if(len(Y_Pred)>0):
                                final=final.append(Y_Pred)
                Result_df.iloc[j,1]=mean_squared_error(Y_Test, final)
                Result_df.iloc[j,2]=r2_score(Y_Test, final)
                plt.figure(figsize=(40,20))
                plt.plot(final[kpi],'--',color='b')
                plt.plot(result[kpi],color='orange')
                plt.axvline(x=final.index[0],color='r')
                plt.xticks(rotation=45)
                if(time_feature==True):
                    if(for_loop==True):
                        name=method+'/WTF/WFL'
                    else:
                        name=method+'/WTF/WOFL'
                else:
                    if(for_loop==True):
                        name=method+'/WOTF/WFL'
                    else:
                        name=method+'/WOTF/WOFL'
                plt.title('Capacity Planning for '+kpi+' With '+method)
                plt.savefig('Plots/'+name+'/Capacity Planning for '+kpi+' With '+method+'.jpg')
            except:
                print('Cannot Do for '+kpi)    
        Result_df.to_csv('Logs/'+name.replace('/','_')+'.csv')
    else:
        print('No Kpis Columns Available')
            