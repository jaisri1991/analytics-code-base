def CP_main(df,peak_data,n):
    ree=replace_outlier_to_Nan(df,peak_data)
    cols = [col for col in df.columns if col not in ['Time']]
    imputate_data=impute_using_sma(ree,cols,n)
    return imputate_data