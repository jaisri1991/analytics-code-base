# os.chdir('../../')
import os
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '/Src'
Models_Dir = Base_Dir + '/Models'
exec(open(Base_Dir+'/Include/include.py').read())

data,comp_info=fetch_data(hostip,2,'2017-03-01','2017-04-31',comp_list,txn_list,'Total,AvgRespTime,SlowPercentage')
ind = DataFrame({'Time':pd.date_range(data.Time.min(), data.Time.max(), freq=str(1) + 'min')})
dd=ind.merge(data,on='Time',how='outer')
cols = [col for col in dd.columns if col not in ['Time']]
dd=dd.fillna(method='ffill')
dd=dd.fillna(method='bfill')
dd=dd.fillna(0)
dd.index=dd.Time
df = non_overlapping_average(dd,60,aggr_method='mean')

ped=pd.read_csv(Base_Dir+'/Data/RIB/peak_data.csv')
ped.Time=pd.to_datetime(peak_data.Time)
peak_data=ped[(ped.Time>='2017-01-24') & (ped.Time<='2017-02-06 23:00:00')]
n=3
win_len=60
percentage=0.9
nhead=5
imputate_data=CP_main(df,peak_data,n)
accuracy_for_all(imputate_data,method='RF',percentage=0.9,time_feature=True,for_loop=True,nhead=5)