#@profile
def calcInputDateRange(app_id):
    sql_string = 'select max(Time) as Latest_Results_Timestamp, App_Id, Comp_Id, Metric_Name ' \
                 'from AD_RT_ANOMALYDEF where App_Id = ' + str(app_id)
    time_df=db_result(sql_string)
    if(len(time_df)>0):
        #Last refresh date
        last_refreshed = time_df['Latest_Results_Timestamp'].min().to_pydatetime()
        return last_refreshed
    else:
        return default_to



#Following function converts a n row data frame (with 'Time' as the time stamp in one column) to n//window_len rows of dataframe
#Only averaging is supported for now
#The starting time along with window length (in minutes) ditates the rows of result dataframe
#The window is right adjusted so that the first point in the result data frame comes from (min_time + window_len)th point of the original dataframe
#raw_df is assumed to have minutely data
#impute_then_downsample is a flag which dictates if imputation needs to be done at a minute level before averaging or averaging can be done first
#If averaging is done first then we will need to perform lesser number of imputations
#The sequencing is to be preseved i.e time indices should be row0 - earliest recorded to rown - latest recorded
#@profile
def non_overlapping_average(raw_df,window_len,aggr_method='mean',impute_then_downsample=False):
    averaged_df=DataFrame()
    if((isinstance(raw_df.index,pd.core.indexes.datetimes.DatetimeIndex))):
        if (aggr_method == 'mean'):
            averaged_df = raw_df.resample(str(window_len)+'Min').mean()
        elif (aggr_method == 'median'):
            averaged_df = raw_df.resample(str(window_len)+'Min').median()
        elif (aggr_method == 'skew'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.skew)
        elif (aggr_method == 'kurt'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.kurt)
    else:
        print('DataFrame Index is not DateTime')
	
    #Impute data
    if(impute_then_downsample== True): 
        averaged_df['Time']=averaged_df.index
        rng = pd.DataFrame(pd.date_range(start = min(averaged_df.index), end=max(averaged_df.index), freq=str(window_len)+'min'),columns=['Time'])
        result = rng.merge(averaged_df,how='outer',on='Time')
        cols = [col for col in result.columns if col not in ['Time']]
        #result.fillna(method='ffill',inplace=True)
        result=impute_using_sma(result,cols,3)
        result=result.fillna(method='ffill')
        result = result.fillna(result.mean())
        averaged_df=result[:]
    return averaged_df

#The following function will fill the NaNs with the above row mean values based on the given length column by column
# X is the dataframe which we pass with NaNs
# cols is the column names
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value
#@profile
def impute_using_sma(x,cols,n):
    t1 = time.time()
    print('Imputation Process Started')
    if(isinstance(cols,list)):
        for f in cols:
          #print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    print("Total Time Taken For Imputation Process: %s" % (time.time() - t1))
    return x


#The following function will fill the NaNs with the above row mean values based on the given length
# X is the dataframe which we pass with NaNs
# f is the column name
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isnull(x.ix[0,f]) == True):
        nn = x[f].notnull().nonzero()[0]
        if (len(nn) > 1):
            x.ix[0, f]=x.ix[nn[0]:nn[1]+1, f].mean()
        else:
            x.ix[0, f] =0
    if (count > n):
        ### Method is SMA
        row_no = pd.isnull(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if((len(row_no)==count-1)&(x.ix[0, f]==0)):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()

def apply_weightage(data,high=None,med=None,low=None):
    high_cols=data.filter(regex='High').columns
    med_cols=data.filter(regex='Medium').columns
    low_cols=data.filter(regex='Low').columns
    dd=data[:]
    #If the columns are applicable and if the values if set for high/med/low then those weightages will be applied to the dataframe
    if((len(high_cols)>0)&(not not high)):
        dd[high_cols]=dd[high_cols]*high
    if((len(med_cols)>0)&(not not med)):
        dd[med_cols]=dd[med_cols]*med
    if((len(low_cols)>0)&(not not low)):
        dd[low_cols]=dd[low_cols]*low
    if((len(high_cols)==0)&(len(med_cols)==0)&(len(low_cols)==0)):
        dd=dd*.75
    #Extracting Unique Column Names
    uni_cols=dd.rename(columns=lambda x: str(x).replace('_High', '').replace('_Medium', '').replace('_Low', '')).columns.unique()
    #Summing up all the kpis weightages as one column instead of three(high/med/low)
    for c in uni_cols:
        sum_value=DataFrame({c:dd.filter(regex=c).sum(axis=1)})
        if(c==uni_cols[0]):
            Result=sum_value
        else:
            Result[c]=sum_value
    Result.index=dd.index
    return Result

#@profile
def data_plot(data_set,pred,kpi,anom_indices):
    mean_pred = pd.Series(pred['Mean'],index=pred.index,name='Mean_Pred')
    plt.plot(mean_pred, label=str(nhead) + '-step ahead Forecast',color='orange',alpha=0.5)

    ax = data_set.plot(label='observed')
    pred_ci = pred[['lower '+kpi,'upper '+kpi]]
    ax.fill_between(pred_ci.index,
                    pred_ci.iloc[:, 0],
                    pred_ci.iloc[:, 1], color='blue', alpha=.1)
    ax.set_xlabel('Date')
    ax.set_ylabel('Metric')
    plt.legend()

    for count in range(len(anom_indices)):
        ax.scatter(data_set[anom_indices].index[count],data_set[anom_indices[count]],color='red',alpha=0.8)

    plt.savefig("output/charts/" + kpi + "_nsteps" + str(nhead) +".png")
    plt.close()


class datetime_mod(datetime.datetime):
    def __divmod__(self, delta):
        seconds = int((self - datetime.datetime.min).total_seconds())
        remainder = datetime.timedelta(
            seconds=seconds % delta.total_seconds(),
            microseconds=self.microsecond,
        )
        quotient = self - remainder
        return quotient, remainder

    def __floordiv__(self, delta):
        return divmod(self, delta)[0]

    def __mod__(self, delta):
        return divmod(self, delta)[1]
