def db_result(query):
    conn=dbfun()
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        df=pd.DataFrame(rows)
        if(len(df)>0):
            df.columns=[i[0] for i in cursor.description]
            print('Total Row(s):', cursor.rowcount)
        return df
    except Exception as e:
        print(e)
    finally:
        cursor.close()


def CLS_update_table(df,tab):
    t0=time.time()
    conn=dbfun()
    try:
        cursor=conn.cursor()
        #cursor.execute('CREATE TABLE IF NOT EXISTS `AD_RT_ANOMALYDEF` (`Time` DATETIME NOT NULL,`App_Id` INT NOT NULL,`Comp_Id` INT NOT NULL,`Layer` VARCHAR(20) NOT NULL,`Win_Len` INT NOT NULL,`Comp_Name` VARCHAR(100) NOT NULL,`Metric_Name` VARCHAR(100) NOT NULL,`Anomaly` INT NULL,`Mean` FLOAT NULL,`Lower` FLOAT NULL,`Upper` FLOAT NULL,PRIMARY KEY (`Time`, `App_Id`, `Comp_Id`, `Win_Len`,`Comp_Name`,`Metric_Name`));')
        #for i in range(len(df)):
        #    try:
        #        df.iloc[i:i + 1].to_sql(name="AD_RT_ANOMALYDEF", if_exists='append', con=engine,index=False)
        #    except:
        #        val=df.iloc[i:i + 1]
        #        query = "delete from AD_RT_ANOMALYDEF where Time='%s' and App_Id=%s and Comp_Id=%s and Comp_Name='%s' and Metric_Name='%s'" % (
        #            val.Time.dt.strftime('%Y-%m-%d %H:%M:%S').values[0],val.App_Id.item(), val.Comp_Id.item(), val.Comp_Name.item(), val.Metric_Name.item())
        #        cursor.execute(query)
        #        conn.commit()
        #        val.to_sql(name="AD_RT_ANOMALYDEF", if_exists='append', con=engine,index=False)

        #df.Time=df.Time.apply(lambda x: x.strftime('%Y-%m-%d %H:%M:%S'))

        if(tab=='ANO'):
            cols = [col for col in df.columns if col not in ['Time', 'CompInstanceId']]
            data = df.set_index(['Time'])[cols].to_records().tolist()
            dbquery = """insert into AD_RT_ANOMALYDEF values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE  Time=VALUES(Time),App_Id=VALUES(App_Id),Comp_Id=VALUES(Comp_Id),Win_Len=VALUES(Win_Len), Comp_Name=VALUES(Comp_Name), Metric_Name=VALUES(Metric_Name), Anomaly=VALUES(Anomaly),Mean=VALUES(Mean), Lower=VALUES(Lower),Upper=VALUES(Upper),Layer=VALUES(Layer),Actual_Value=VALUES(Actual_Value),AD_Method=VALUES(AD_Method);"""
        elif(tab=='CLF'):
            data = df.to_records(index=False).tolist()
            dbquery = """insert into AD_RT_CLF_Moments_CONFIG values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE  StartTime=VALUES(StartTime),EndTime=VALUES(EndTime),AppInstanceId=VALUES(AppInstanceId),TxnId=VALUES(TxnId), Metric_Name=VALUES(Metric_Name), Sum_M3_Square=VALUES(Sum_M3_Square), Sum_M4_Square=VALUES(Sum_M4_Square),Mean_M3=VALUES(Mean_M3), Mean_M4=VALUES(Mean_M4), nM3=VALUES(nM3), nM4=VALUES(nM4);"""
        try:
            cursor.executemany(dbquery,data)
            conn.commit()
        except:
            conn.rollback()
        print("Total Time Taken To Update Is %s"%(time.time()-t0))
    except Exception as e:
        print(e)
    finally:
        cursor.close()