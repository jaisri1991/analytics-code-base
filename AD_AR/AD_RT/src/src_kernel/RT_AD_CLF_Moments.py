def AD_RT_CLF_Moments_Wrapper(raw_df,comp_info,win_len,app_id,threshold):
    #frm='2017-03-06 17:00:00'
    #to='2017-03-06 17:59:00'
    #final_result=DataFrame()
    #for i in range(0,480):
     #   if(i==0):
     #       result=AD_RT_CLF_Moments(frm,to,win_len,app_id,threshold)
     #   else:
     #       frm=str(datetime.datetime.strptime(max(final_result.Time),"%Y-%m-%d %H:%M:%S")+datetime.timedelta(minutes=60))
     #       to=str(datetime.datetime.strptime(frm,"%Y-%m-%d %H:%M:%S")+datetime.timedelta(minutes=59))
    result=AD_RT_CLF_Moments(raw_df,comp_info,win_len,app_id,threshold)
#        final_result = final_result.append(result, ignore_index=True)
    
    return result
            
        
 
def AD_RT_CLF_Moments(raw_df,comp_info,win_len,app_id,threshold):
    #raw_df,comp_info=fetch_data(hostip,app_id,frm,to,comp_list,txn_list,'AvgRespTime,SlowPercentage')
#Updating the Square and Mean values for the transaction with the new data points
    clf_df=AD_RT_CLF_Moments_Calc(raw_df,comp_info,win_len,app_id,'realtime')
    df = non_overlapping_average(raw_df,win_len,aggr_method='mean')
    cmquery='select * from AD_RT_CLF_Moments_CONFIG'
    cm_data=db_result(cmquery)
    txn_cols = [col for col in df.columns if 'Txn' in col]
    df_result=DataFrame()
    for col in txn_cols:
        #Fetching Square and Mean data for individual transactions
        x_data=cm_data[(cm_data.Metric_Name==col)]
        #Extracting Updated Square Value
        x_square_by_n=np.array(x_data.Sum_M3_Square)[0]/np.array(x_data.nM3)[0]
        #Extracting Updated Mean Value
        mean_square=round(np.square(np.array(x_data.Mean_M3)[0]),45)
        #Calculating SD with new values
        Variance=x_square_by_n-mean_square
        if(Variance==0):
            SD=1e-44
        else:
            SD=np.sqrt(Variance)
        moment_3 = stats.moment(raw_df[0:60][col],moment=3)
        moment_4 = stats.moment(raw_df[0:60][col],moment=4)
            
        txn_normalize=(moment_3-np.array(x_data.Mean_M3)[0])/SD
        
        comp_name=col.replace('Txn_','').replace('_Total','').replace('_AvgRespTime','').replace('_SlowPercentage','')
        try:
            comp_id=comp_info[comp_info.Name == comp_name].CompId.item()
            Lay='TXN'
            met_name=col
            if((txn_normalize > threshold) | (txn_normalize < -threshold)):
                ano=1
            else:
                ano=0
            kpi = pd.DataFrame({'Time': str(df.index[0])+':00:00', 'Win_Len': win_len, 'App_Id': app_id,
                                'Comp_Id': comp_id, 'Comp_Name': comp_name,
                                'Layer': Lay,'Metric_Name': met_name, 'Anomaly': ano,'AD_Method':'M002',
                                'Mean': -1, 'Lower':-1,
                                'Upper': -1,'Actual_Value':df[col][0]},index=[0])
            df_result = df_result.append(kpi, ignore_index=True)
        except:
            print('No Data for '+col)
    result = df_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','AD_Method']]
    return result
    
def AD_RT_CLF_Moments_Calc(raw_df,comp_info,win_len,app_id,stage='realtime'):
    cquery=('select * from AD_RT_CLF_Moments_CONFIG')
    clf_data=db_result(cquery)
    df = non_overlapping_average(raw_df,1,aggr_method='mean')
    txn_cols = [col for col in df.columns if 'Txn' in col]
    clf_df=DataFrame(columns=['StartTime','EndTime','AppInstanceId','TxnId','Metric_Name','Sum_M3_Square','Sum_M4_Square','Mean_M3','Mean_M4','nM3','nM4'])
    for col in txn_cols:
        #print('Calculating for %s'%col)
        ts = df[col]
        dist_metrics_cols = [col + '_mnt_3', col + '_mnt_4']
        dist_metrics_df = pd.DataFrame(columns=dist_metrics_cols)
        seq_len = (len(ts) // win_len)+1

        for i in range(seq_len):

            moment_3 = stats.moment(ts[i*win_len:(i+1)*win_len],moment=3)
            moment_4 = stats.moment(ts[i*win_len:(i+1)*win_len], moment=4)
            win_dist_metrics = pd.Series([moment_3,moment_4],index=dist_metrics_cols)
            dist_metrics_df = dist_metrics_df.append(win_dist_metrics, ignore_index=True)

        ind = pd.date_range(ts.index.min(), periods=dist_metrics_df.shape[0], freq=str(win_len) + 'min')
        dist_metrics_df.index = ind
        comp_name=col.replace('Txn_','').replace('_AvgRespTime','').replace('_SlowPercentage','')
        if(col==txn_cols[0]):
            moments=dist_metrics_df
        else:
            moments=moments.join(dist_metrics_df)
        try:
            comp_id=comp_info[comp_info.Name == comp_name].CompId.item()            
            if((stage=='realtime') & (len(clf_data)>0 )):
                frm=str(min(clf_data.StartTime))
                to=str(max(raw_df.Time))
                mean_and_stds=clf_data[(clf_data.AppInstanceId == app_id) & (clf_data.TxnId==comp_id) & (clf_data.Metric_Name == col)]
                if(len(mean_and_stds)>0):
                    sm3=(np.square(dist_metrics_df[dist_metrics_cols[0]])+mean_and_stds.Sum_M3_Square).sum()
                    sm4=(np.square(dist_metrics_df[dist_metrics_cols[1]])+mean_and_stds.Sum_M4_Square).sum()
                    nM3=np.array(mean_and_stds.nM3+len(ts))[0]
                    nM4=np.array(mean_and_stds.nM4+len(ts))[0]
                    m3=(np.array(mean_and_stds.Mean_M3*mean_and_stds.nM3)[0]+np.array(dist_metrics_df[dist_metrics_cols[0]])[0])/nM3
                    m4=(np.array(mean_and_stds.Mean_M4*mean_and_stds.nM4)[0]+np.array(dist_metrics_df[dist_metrics_cols[1]])[0])/nM4
                    txn_data={'StartTime':frm,'EndTime':to,'AppInstanceId':app_id,'TxnId':comp_id,'Metric_Name':col,'Sum_M3_Square':sm3,'Sum_M4_Square':sm4,'Mean_M3':m3,'Mean_M4':m4,'nM3':nM3,'nM4':nM4} 
            elif(stage=='baseline'):
                frm=str(min(raw_df.Time))
                to=str(max(raw_df.Time))
                sm3=np.square(dist_metrics_df[dist_metrics_cols[0]]).sum()
                sm4=np.square(dist_metrics_df[dist_metrics_cols[1]]).sum()
                m3=dist_metrics_df[dist_metrics_cols[0]].mean()
                m4=np.array(dist_metrics_df[dist_metrics_cols[1]])[0].mean()
                nM3=len(dist_metrics_df[dist_metrics_cols[0]])
                nM4=len(dist_metrics_df[dist_metrics_cols[1]])
                txn_data={'StartTime':frm,'EndTime':to,'AppInstanceId':app_id,'TxnId':comp_id,'Metric_Name':col,'Sum_M3_Square':sm3,'Sum_M4_Square':sm4,'Mean_M3':m3,'Mean_M4':m4,'nM3':nM3,'nM4':nM4}
            clf_df=clf_df.append(txn_data,ignore_index=True)
        except:
            print('Skipping for %s Since there is no data'%col)
    CLS_update_table(clf_df,'CLF')
    return clf_df
    
        
        