#@profile
def main():
    # Source Files
    # Calling Functions
    pt0=time.process_time()
    print('Start Time For Main Function: ' + str(datetime.datetime.now()))
    print('Process Time Before Starting Main Function: '+ str(time.process_time()-pt0))
    
    pt1 = time.process_time()
    last_refreshed = calcInputDateRange(app_id)
    frm = (pd.to_datetime(last_refreshed) - datetime.timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
    to = (pd.to_datetime(last_refreshed) + datetime.timedelta(minutes=59)).strftime('%Y-%m-%d %H:%M:%S')
    print('Fetching Data From %s and To %s'%(frm,to))

    data_set,comp_info=fetch_data(hostip,app_id,frm,to,comp_list,txn_list)
    print("Dimension Of The Input Dataset: Rows: %d, Cols: %d and Min Time is %s and Max Time is %s"%(data_set.shape[0],data_set.shape[1],min(data_set.Time),max(data_set.Time)))
    print('Process Time For Fetch Data Function: ' + str(time.process_time()-pt1))
    #Main real time module
    pt1 = time.process_time()
    data_set.index=data_set.Time
    ADR_result = AD_Realtime_Main(data_set,bl,roll_up_size,app_id,comp_info,nhead)
    print("Dimension Of The Result Dataset: Rows: %d, Cols: %d"%(ADR_result.shape[0],ADR_result.shape[1]))
    print('Process Time For AD_Realtime Function: ' + str(time.process_time()-pt1))
    
    print('End Time For Main Function: ' + str(datetime.datetime.now()))
    print('Total Process Time At The End Of Main Function: ' + str(time.process_time()-pt0))
    return data_set,comp_info,ADR_result#,AD_CL_Result,AD_CO_Result