#@profile
def AD_Realtime_Main(data_set,bl,roll_up_size,app_id,comp_info,nhead):

    cols = [col for col in data_set.columns if col not in ['Time','CompInstanceId']]
    data_set[cols] = data_set[cols].apply(pd.to_numeric,errors='coerce')

    #Overlapping average followed by sampling
    ts_obj=non_overlapping_average(data_set,roll_up_size,impute_then_downsample=True)
    #Index with datetime index
    ts_obj['Time']=ts_obj.index

    #Call AD module
    #t1=time.time()
    AD_result = AD_RT_SARIMAX_Wrapper(ts_obj,bl,roll_up_size,app_id,comp_info,nhead)
    #print("Time Taken To Detect Anomalies Realtime: %s" % (time.time() - t1))
    return AD_result

#@profile
def AD_RT_SARIMAX_Wrapper(ts_obj,bl,roll_up_size,app_id,comp_info,nhead):

    cols = [col for col in ts_obj.columns if col not in ['Time','CompInstanceId']]
    df_result = pd.DataFrame()
    for i in cols:
        try:
            pqd = bl[bl.Metric_Name == i].PQD.item()
            pqd=pqd.split(',')
            pqd=pd.to_numeric(pqd)
            pqd=tuple(pqd)
            df = ts_obj[i]
            if(i[0:3] != 'Txn'):
                comp_id = int(bl[bl.Metric_Name == i].CompInstanceId.item())
                Lay = comp_info[comp_info.CompId == comp_id].Layer.item()
                comp_name = comp_info[comp_info.CompId == comp_id].Name.item()
                met_name = df.name.replace(comp_name + '_', '')
                df.name = met_name
            else:
                comp_name=i.replace('Txn_','').replace('_Total','').replace('_AvgRespTime','').replace('_SlowPercentage','')
                comp_id=comp_info[comp_info.Name == comp_name].CompId.item()
                Lay='TXN'
                met_name=i
            #Using SARIMAX Function
            t1=time.time()
            try:
                pred = AD_RT_forecast(df,nhead,pqd,train_test_split=1)
                count = len(pred)
                kpi = pd.DataFrame({'Time': pred.index, 'Win_Len': [roll_up_size] * count, 'App_Id': [app_id] * count,
                                    'Comp_Id': [comp_id] * count, 'Comp_Name': [comp_name] * count,
                                    'Layer': [Lay] * count,'Metric_Name': [met_name] * count, 'Anomaly': -1})
                kpi_range = pd.DataFrame(
                    {'Time': pred.index, 'Mean': pred.Mean, 'Lower': pred['lower ' + met_name],
                     'Upper': pred['upper ' + met_name],'Actual_Value':-1,'AD_Method':'M001'})
                kpi_result = pd.merge(kpi, kpi_range, on='Time', how='outer')
                df_result = df_result.append(kpi_result, ignore_index=True)
            except:
                print('Cannot Do Realtime For %s, The PQD Value is %s'%(i,pqd))

        except:
             print("Cannot Do Realtime For %s Since There Is No PQD Data Available In DB"%i)
        #Create the result data frame

    result = df_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','AD_Method']]
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    CLS_update_table(result,'ANO')
    return result

def AD_RT_forecast(df,nhead,pqd,train_test_split = 0.8):
    X_train=df[:(len(df)*train_test_split).__round__()]
    X_test=df[(len(df)*train_test_split).__round__():]
    test_result=pd.DataFrame()
    obs = np.zeros(nhead)
    if(len(X_test) == 0):
        results = AD_SARIMAX_fit(X_train, pqd)
        obs = np.zeros(nhead)
        pred = AD_SARIMAX_forecast(results, nhead)
        pred_mean=pd.DataFrame({'obs_'+df.name:obs,'Mean':pred.predicted_mean})
        dff=pd.concat([pred_mean,pred.conf_int(alpha=0.003)],axis=1)
        test_result=test_result.append(dff)
    else:
        for i in range(0,(len(X_test)/nhead).__round__()):
            print("Running detection for kpi: " + df.name)
            if(i==0):
                results = AD_SARIMAX_fit(X_train,pqd)
            else:
                data = df[(i*nhead):(len(X_train)+(i*nhead))]
                results = AD_SARIMAX_fit(data, pqd)
                obs = X_test[i*nhead:(i+1)*nhead]
            gc.collect()
            pred = AD_SARIMAX_forecast(results, nhead)
            print(str(i) + ' Done' + ' Out Of ' + str((len(X_test) / nhead).__round__()))

            pred_mean=pd.DataFrame({'obs_'+df.name:obs,'Mean':pred.predicted_mean})
            dff=pd.concat([pred_mean,pred.conf_int(alpha=0.003)],axis=1)
            test_result=test_result.append(dff)
            test_result=test_result[test_result.index<=X_test.index[-1]]
    return test_result

def AD_SARIMAX_fit(ts_obj,pqd):
    if (len(pqd) > 3):
        mod = sm.tsa.statespace.SARIMAX(ts_obj,
                                        order=(int(pqd[0]),int(pqd[1]),int(pqd[2])),
                                        seasonal_order=(int(pqd[3]),int(pqd[4]),int(pqd[5]),int(pqd[6])),
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)
    else:
        mod = sm.tsa.statespace.SARIMAX(ts_obj,
                                        order=(int(pqd[0]),int(pqd[1]),int(pqd[2])),
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)

    results = mod.fit(maxiter=20,disp=0)
    gc.collect()

    return results

def AD_SARIMAX_forecast(results,nhead):
    pred=results.get_forecast(nhead)
    gc.collect()
    return pred

