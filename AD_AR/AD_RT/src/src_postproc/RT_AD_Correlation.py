def AD_RT_Correlation_Main(app_id,frm,to,layer,win_len=3,high=0.75,med=0.2,low=0.05,fast=False):
    m_frm=(pd.to_datetime(frm) - datetime.timedelta(hours=win_len)).strftime('%Y-%m-%d %H:%M:%S')
    dquery="select * from AD_RT_ANOMALYDEF where Time between '"+m_frm+"' and '"+to+"' and App_Id="+str(app_id)
    d=db_result(dquery)
    d['Name']=d.Metric_Name
    d['Name'][d[d.Layer!='TXN'].index]=d[d.Layer!='TXN'].Comp_Name+'_'+d[d.Layer!='TXN'].Metric_Name
    dd=d.pivot(index='Time', columns='Name', values='Anomaly').fillna(0)
    cols = [col for col in dd.columns if col not in ['Time']]
    ema_data = dd[cols].ewm(span = win_len,adjust = False).mean()
    ema_data.index = dd.index
    Result = apply_weightage(ema_data,high,med,low)
    #Result = Result[Result.index >= Date1]
    Result=Result[Result.sum()[Result.sum() > 0].index]
    Corr_Kpis=DataFrame()

    if(len(Result) > 0):
        if(fast==True):
            Corr=pd.DataFrame(index=Result.columns,columns=Result.columns)
            for i in Result.columns:
                for j in Result.columns:
                    distance,pa=fastdtw(Result[i],Result[j],dist=euclidean)
                    Corr[i][j]=distance
        else:
            Corr = Result.corr()
        for kpi in Result.columns:
            sub=d[d.Name==kpi]
            if(fast==True):
                h = Corr[kpi].sort_values(ascending=True)#.drop(labels=kpi)
            else:
                h = Corr[kpi].sort_values(ascending=False)#.drop(labels=kpi)
            h.dropna(inplace=True)
            if (len(h) > 0):
                co=d[d.Name.isin(h.index)]
                co=co[['App_Id','Layer','Comp_Name','Comp_Id','Metric_Name','Name']]
                co=co.drop_duplicates()
                c = DataFrame({'SubjectKPI_AppId':sub.App_Id.values[0], 'SubjectKPI_Layer':sub.Layer.values[0], 'SubjectKPI_CompId':sub.Comp_Id.values[0],'SubjectKPI_CompName':sub.Comp_Name.values[0],'SubjectKPI_MetricName':sub.Metric_Name.values[0], 'CorrelatedKPI_AppId':co.App_Id,'CorrelatedKPI_Layer':co.Layer, 'CorrelatedKPI_CompId':co.Comp_Id, 'CorrelatedKPI_CompName':co.Comp_Name,'CorrelatedKPI_MetricName':co.Metric_Name,'KPIS':co.Name})	
                c=c.merge(DataFrame({'KPIS':h.index,'CorrelatedKPI_Values':h.values}),on='KPIS',how='inner')
                c=c.sort_values('CorrelatedKPI_Values',ascending=False)
                c.drop('KPIS',axis=1,inplace=True)
            else:
                c = DataFrame({'SubjectKPI_AppId':sub.App_Id.values[0], 'SubjectKPI_Layer':sub.Layer.values[0], 'SubjectKPI_CompId':sub.Comp_Id.values[0],'SubjectKPI_CompName':sub.Comp_Name.values[0],'SubjectKPI_MetricName':sub.Metric_Name.values[0], 'CorrelatedKPI_AppId':sub.App_Id.values[0],'CorrelatedKPI_Layer':sub.Layer.values[0], 'CorrelatedKPI_CompId':sub.Comp_Id.values[0], 'CorrelatedKPI_CompName':sub.Comp_Name.values[0],'CorrelatedKPI_MetricName':sub.Metric_Name.values[0],'CorrelatedKPI_Values':1},index=[0])
            if((kpi==Result.columns[0]) | (len(Corr_Kpis)==0)):
                Corr_Kpis=c
            else:
                Corr_Kpis=Corr_Kpis.append(c)
           # else:
                #print('None of the KPIs Correlated For ' + kpi) 
                
    if(len(Corr_Kpis)>0):
        Corr_Kpis = Corr_Kpis[['SubjectKPI_AppId','SubjectKPI_Layer','SubjectKPI_CompId','SubjectKPI_CompName','SubjectKPI_MetricName', 'CorrelatedKPI_AppId','CorrelatedKPI_Layer', 'CorrelatedKPI_CompId', 'CorrelatedKPI_CompName','CorrelatedKPI_MetricName','CorrelatedKPI_Values']]
        Corr_Kpis.index = range(0, len(Corr_Kpis))
    #ui_filter=(Corr_Kpis['SubjectKPI_Layer'] == layer) & (Corr_Kpis['CorrelatedKPI_Layer'] == layer) & (Corr_Kpis['CorrelatedKPI_Layer'] == layer)
    ui_filter=(Corr_Kpis['CorrelatedKPI_Layer'] == layer)
    return Corr_Kpis[ui_filter]

def AD_RT_Correlation_Wrapper(app_id,frm,to,layer,total_kpis=6,DCK_kpis=4,win_len=3,high=0.75,med=0.2,low=0.05,fast=False):
    if(total_kpis>DCK_kpis):
        DCK=AD_RT_Correlation_Main(app_id,frm,to,layer,win_len=3,high=0.75,med=0.2,low=0.05,fast=False)
        squery='select * from AD_RT_Static_Correlated where SubjectKPI_Layer="'+layer+'" and CorrelatedKPI_Layer="'+layer+'" and SubjectKPI_AppId='+str(app_id)
        SCK=db_result(squery)
        if(len(DCK)>DCK_kpis):
            DCK=DCK[0:DCK_kpis]
        Total=DCK.append(SCK)
        #Total=Total.sort_values('CorrelatedKPI_Values',ascending=False)
        Total.index=range(0,len(Total))
        Sub=Total.iloc[:,0:10].drop_duplicates()
        Re=DataFrame(Sub.iloc[:,5:10].values,Sub.iloc[:,0:5].values,columns=Sub.iloc[:,5:10].columns)
        Re=Re.drop_duplicates()
        Re.index=range(0,len(Re))
        return Re[0:total_kpis-1]
    else:
        print('DCK number is bigger than the Total number of KPIs')