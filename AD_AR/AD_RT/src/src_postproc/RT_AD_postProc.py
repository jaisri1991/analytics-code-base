def AD_RT_Classifier(bl,roll_up_size,aggr_method='mean'):
    t0=time.time()
    pt0 = time.process_time()
    pt1 = time.process_time()
    print('Start Time For Classifier: '+str(datetime.datetime.now()))
    print('Process Time Before Starting Classifier Function: ' + str(time.process_time()-pt0))
    final_result=DataFrame()
    result=DataFrame()
    # Get max and min of datetime corresponding to Anomaly Flag = -1 (yet to be classified), unique (app_id,comp_id, metric_name)
    try:
        t1=time.time()

        aquery='select * from AD_RT_ANOMALYDEF where Anomaly=-1'
        ano_data=db_result(aquery)
        if(len(ano_data)>0):
            ano_data['Anomaly'] = 2
            CLS_update_table(ano_data,'ANO')
            print("Time Taken To Fetch Anomaly Details From DB Is %s"%(time.time()-t1))

            try:
            # Form url to call java service to get minutely data, roll up consistently using roll_up size and right alignement
                pt1=time.process_time()
                acfrm=str((min(ano_data.Time))) #- datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S'))
                acto=str((max(ano_data.Time) + datetime.timedelta(minutes=59)).strftime('%Y-%m-%d %H:%M:%S'))
                app_id = ano_data.App_Id.unique()[0]
                co_list=ano_data[ano_data.Layer != 'TXN'].Comp_Id.unique()
                if(len(co_list)>0):
                    co_list=','.join(str(x) for x in co_list)
                else:
                    co_list=''
                txn_list = ano_data[ano_data.Layer == 'TXN'].Comp_Id.unique()
                if(len(txn_list)>0):
                    txn_list=','.join(str(x) for x in txn_list)
                else:
                    txn_list=''
                print('Fetching Data From %s and To %s' % (acfrm, acto))
                ac_data_set, ac_comp_info = fetch_data(hostip, app_id, acfrm, acto, co_list,txn_list)
                print("Dimension Of The Input Dataset: Rows: %d, Cols: %d and Min Time is %s and Max Time is %s" % (ac_data_set.shape[0], ac_data_set.shape[1], min(ac_data_set.Time),max(ac_data_set.Time)))  # Compare with the corresponding lower and upper boundaries and classify
                print('Process Time For Fetch Data: ' + str(time.process_time()-pt1))
                if(len(ac_data_set)>0):
                    mquery='select * from AD_RT_ALGORITHMDEF'
                    method_data=db_result(mquery)
                    m1=method_data[(method_data.Method=='M001')].Metric_Name.values.tolist()
                    m1=ac_data_set.columns[ac_data_set.columns.isin(m1)].values
                    m1=np.append(m1,['Time'])
                    m2=method_data[(method_data.Method=='M002')].Metric_Name.values.tolist()
                    m2=ac_data_set.columns[ac_data_set.columns.isin(m2)].values
                    m2=np.append(m2,['Time'])
                    t1=time.time()
                    if(len(m1)>0):
                        
                        hour_data=RD_Convert(ac_data_set[m1],roll_up_size,aggr_method)
                        print("Time Taken To Convert Based On Window Length %s" % (time.time() - t1))
                        t1=time.time()
                        final_result=AD_RT_Wrapper(hour_data,ano_data,bl,app_id,ac_comp_info)
                        m1_final_result = final_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','AD_Method']]
                        CLS_update_table(m1_final_result,'ANO')
                        result=result.append(m1_final_result)
                    if(len(m2)>0):
                        
                        m2_final_result=AD_RT_CLF_Moments_Wrapper(ac_data_set[m2],ac_comp_info,roll_up_size,app_id,threshold=2.5)
                        CLS_update_table(m2_final_result,'ANO')
                        result=result.append(m2_final_result)
                    print("Time Taken To Update Anomaly Details Is %s" % (time.time() - t1))
                else:
                    #ano_data['Anomaly'] = 2
                    #CLS_update_table(conn, ano_data,'ANO')
                    print('No Data For The Selected Date Range')
                print('Total Process Time At The End Of Classifier Function: ' + str(time.process_time()-pt0))
            except:
                print("Please Check Data Service Is Running")
        else:
            print('No Rows In AD_RT_ANOMALYDEF Table is -1')
    except:
        print("Check The DB Connection")
    #conn.close()
    print("Total Time Taken Is %s" % (time.time() - t0))
    print('End Time For Classifier: ' + str(datetime.datetime.now()))
    return result

def RD_Convert(ac_data_set,roll_up_size,aggr_method):
    ac_data_set.index=ac_data_set['Time']
    cols = [col for col in ac_data_set.columns if col not in ['Time','CompInstanceId']]
    ac_data_set[cols] = ac_data_set[cols].apply(pd.to_numeric,errors='coerce')

    #Overlapping average followed by sampling
    ts_obj=non_overlapping_average(ac_data_set,roll_up_size,aggr_method,impute_then_downsample=False)
    #Index with datetime index
    
    ts_obj['Time'] = ts_obj.index

    return ts_obj

def AD_RT_Wrapper(hour_data,ano_data,bl,app_id,comp_info):
    t0=time.time()
    cols = [col for col in hour_data.columns if col not in ['Time','CompInstanceId']]
    df_result = pd.DataFrame()
    for i in cols:
        t1 = time.time()
        try:
            df = hour_data[['Time',i]]
            if(i[0:3] != 'Txn'):
                comp_id = int(bl[bl.Metric_Name == i].CompInstanceId.item())
                comp_name = comp_info[comp_info.CompId == comp_id].Name.item()
                met_name = df.columns[1].replace(comp_name + '_', '')
                df.columns.values[1] = 'Actual_Value'
            else:
                met_name=i
                comp_name=i.replace('Txn_','').replace('_Total','').replace('_AvgRespTime','').replace('_SlowPercentage','')
                comp_id=comp_info[comp_info.Name == comp_name].CompId.item()
                df.columns.values[1] = 'Actual_Value'

            r_data=ano_data[(ano_data.App_Id==app_id)&(ano_data.Comp_Id==comp_id)&(ano_data.Metric_Name==met_name)]
            r_data.pop('Actual_Value')
            f_data=pd.merge(df,r_data,how='inner',on='Time',right_index=True)

            anom_indices = f_data[(f_data['Actual_Value'] < f_data['Lower']) |
                                (f_data['Actual_Value'] > f_data['Upper'])].index
            index = f_data[f_data.Time.isin(anom_indices)].index
            f_data['Anomaly']=0
            f_data['Anomaly'][index] = 1
            df_result = df_result.append(f_data, ignore_index=True)
            #print("Updated For %s in %s" %(i,(time.time()-t1)))
        except:
             print("Cannot Update Anomaly Details For %s "%i)
        #Create the result data frame

    result = df_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','AD_Method']]
    print("Total Time Taken To Update Anomaly Data: %s" % (time.time() - t0))
    return result

def AD_Correlation(app_id, subjectkpi_layer, time_from, time_to,roll_up_size):
    conn = mysqlcon.connect(user='root',password='',host='192.168.13.82',database='APPNOMIC')
    table = 'AD_RT_ANOMALYDEF'
    cursor = conn.cursor(buffered=True)
    print(time_from)
    #Get subject kpi list
    kpi_list_qry =  "select distinct METRIC_NAME from " + str(table) + \
                    " where app_id = " + str(app_id) + \
                    " and win_len=" + str(roll_up_size) + \
                    " and Anomaly = 1" + \
                    " and Time >= '" + str(time_from) + "' and Time < '" + str(time_to) + "'"

    cursor.execute(kpi_list_qry)

    if (cursor.rowcount > 0):

        subject_kpis_df = pd.DataFrame(cursor.fetchall(),columns=cursor.column_names)
        subject_kpis_tuple = tuple(subject_kpis_df.values.flatten())

        #Form the correlation matrix
        in_p = ', '.join(list(map(lambda x: '%s', subject_kpis_tuple)))
        qry_string = "select * from " + str(table) + " where app_id = " + \
                     str(app_id) + " and win_len=" + str(roll_up_size) + \
                     " and Anomaly = 1" + \
                     " and METRIC_NAME in (%s)" + \
                     " and Time >= '" + str(time_from) + "' and Time < '" + str(time_to) + "'"
        cursor.execute(qry_string % in_p, subject_kpis_tuple)

        if (cursor.rowcount > 0):

            AD_timesubset_long = pd.DataFrame(cursor.fetchall(),columns=cursor.column_names)

            #Create composite columns App_Id-Comp_Id-Comp_Name-Metric_Name
            AD_timesubset_long['Composite_Metric_Name'] = 'appid_' + AD_timesubset_long['App_Id'].map(str) + '|' + \
                                                            'layer_' + AD_timesubset_long['Layer'].map(str) + '|' + \
                                                            'compid_' + AD_timesubset_long['Comp_Id'].map(str) + '|' +  \
                                                            'compname_' + AD_timesubset_long['Comp_Name'].map(str) + '|' +  \
                                                            'metricname_' + AD_timesubset_long['Metric_Name'].map(str)
            #AD_timesubset_wide =  AD_timesubset_long.pivot(index=['Time','App_Id','Comp_Id'], columns= 'Metric_Name', values='Anomaly')
            AD_timesubset_wide = pd.pivot_table(AD_timesubset_long,index=['Time'],
                                                          columns=['Composite_Metric_Name'],
                                                          values='Anomaly',
                                                          fill_value=0)
            AD_Correlation = AD_timesubset_wide.corr().fillna(0)

            gc.collect()
            corr_df = pd.DataFrame(columns=['subject_kpi','corr_list'])
            subject_kpis_tuple = tuple(np.unique(AD_timesubset_wide.columns))
            for kpi in subject_kpis_tuple:
                corr_array = pd.Series(AD_Correlation[kpi],name=kpi)
                corr_array.index = AD_Correlation.index
                corr_array.sort_values(ascending=False,inplace=True)
                corr_list = ','.join(map(str, corr_array.index))
                corr_df = corr_df.append(pd.Series({'subject_kpi':kpi,'corr_list':corr_list},name=kpi))

            gc.collect()

            #convert single subject kpi column to multiple columns with information of app instance, comp instance etc.
            corr_df['SubjectKPI_AppId'], \
            corr_df['SubjectKPI_Layer'], \
            corr_df['SubjectKPI_CompId'], \
            corr_df['SubjectKPI_CompName'], \
            corr_df['SubjectKPI_MetricName'] = corr_df['subject_kpi'].str.split('|').str
            del corr_df['subject_kpi']

            #Convert comma separated correlated set of kpis to multiple rows
            lst_col = 'corr_list'
            x = corr_df.assign(**{lst_col: corr_df[lst_col].str.split(',')})
            wide_corr_df = pd.DataFrame({col: np.repeat(x[col].values, x[lst_col].str.len()) for col in x.columns.difference([lst_col])}).assign(**{lst_col: np.concatenate(x[lst_col].values)})[x.columns.tolist()]

            #split piped parts to columns containing app, comp etc. details of correlated kpis
            wide_corr_df['CorrelatedKPI_AppId'], \
            wide_corr_df['CorrelatedKPI_Layer'], \
            wide_corr_df['CorrelatedKPI_CompId'], \
            wide_corr_df['CorrelatedKPI_CompName'], \
            wide_corr_df['CorrelatedKPI_MetricName'] = wide_corr_df['corr_list'].str.split('|').str
            del wide_corr_df['corr_list']

            #Strip unnecessary strings i.e metricname_, appid_, compid_, compname_ etc.
            for col in wide_corr_df.columns:
                lst_substr_to_strip = ['appid_','layer_','compid_','compname_','metricname_']
                for substr in lst_substr_to_strip:
                    wide_corr_df[col] = wide_corr_df[col].str.lstrip(substr)

            #wide_corr_df.to_sql(name="AD_Res_KPICorrelations", if_exists='replace', con=engine, index=False)

            #subset dataframe before returning
            ui_filter = (wide_corr_df['SubjectKPI_Layer'] == subjectkpi_layer) \
                        & (wide_corr_df['CorrelatedKPI_Layer'] == subjectkpi_layer) \
                        & (wide_corr_df['CorrelatedKPI_Layer'] == subjectkpi_layer)
            subset_wide_corr_df = wide_corr_df[ui_filter]
        else:
            cursor.fetchall()
            subset_wide_corr_df = pd.DataFrame({'NoData': []})
    else:
        cursor.fetchall()
        subset_wide_corr_df = pd.DataFrame({'NoData': []})
    conn.close()
    return subset_wide_corr_df
