import memory_profiler
import os
# os.chdir('../../')
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '\\src'

exec(open(Base_Dir+'\include\RT_AD_Sources.py').read())

@profile
def main():
    # Source Files
    #exce(open('G:\Python\AD_AR_v1.2\include\RT_AD_Sources.py').read())
    # Calling Functions
    t0 = time.time()
    print('Start Time For Main Function: ' + str(datetime.datetime.now()))

    #t1=time.time()
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    last_refreshed = calcInputDateRange(conn,app_id)
    frm = (pd.to_datetime(last_refreshed) - datetime.timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
    to = (pd.to_datetime(last_refreshed) + datetime.timedelta(minutes=59)).strftime('%Y-%m-%d %H:%M:%S')
    print('Fetching Data From %s and To %s'%(frm,to))

    data_set,comp_info=fetch_data(hostip,app_id,frm,to,comp_list,txn_list)
    print("Time Taken To Fetch KPI Data From DB: %s"%(time.time()-t1))
    print("Dimension Of The Input Dataset: Rows: %d, Cols: %d and Min Time is %s and Max Time is %s"%(data_set.shape[0],data_set.shape[1],min(data_set.Time),max(data_set.Time)))
    #conn.close()
    #Main real time module
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    ADR_result = AD_Realtime_Main(data_set,bl,roll_up_size,app_id,comp_info,nhead)
    print("Dimension Of The Result Dataset: Rows: %d, Cols: %d"%(ADR_result.shape[0],ADR_result.shape[1]))

    #Classifier Function
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    #AD_CL_Result=AD_RT_Classifier(bl, conn, roll_up_size)

    #Correlation Function
    #t1=time.time()
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    #AD_CO_Result=AD_Correlation(conn,app_id,roll_up_size)
    #print("Time Taken To Correlate Data: %s" % (time.time() - t1))
    #conn.close()
    print("Total Time Taken: %s"%(time.time()-t0))
    print('End Time For Main Function: ' + str(datetime.datetime.now()))
    return data_set,comp_info,ADR_result#,AD_CL_Result,AD_CO_Result

@profile
def AD_RT_Classifier(bl,conn,roll_up_size):
    t0=time.time()
    print('Start Time For Classifier: '+str(datetime.datetime.now()))
    final_result=DataFrame()
    # Get max and min of datetime corresponding to Anomaly Flag = -1 (yet to be classified), unique (app_id,comp_id, metric_name)
    try:
        t1=time.time()
        aquery='select * from AD_RT_ANOMALYDEF where Anomaly=-1'
        ano_data=db_result(conn, aquery)
        if(len(ano_data)>0):
            ano_data['Anomaly'] = 2
            CLS_update_table(conn,ano_data,'ANO')
            print("Time Taken To Fetch Anomaly Details From DB Is %s"%(time.time()-t1))

            try:
            # Form url to call java service to get minutely data, roll up consistently using roll_up size and right alignement

                acfrm=str((min(ano_data.Time))) #- datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S'))
                acto=str((max(ano_data.Time) + datetime.timedelta(minutes=59)).strftime('%Y-%m-%d %H:%M:%S'))
                app_id = ano_data.App_Id.unique()[0]
                co_list=ano_data[ano_data.Layer != 'TXN'].Comp_Id.unique()
                if(len(co_list)>0):
                    co_list=','.join(str(x) for x in co_list)
                else:
                    co_list=''
                txn_list = ano_data[ano_data.Layer == 'TXN'].Comp_Id.unique()
                if(len(txn_list)>0):
                    txn_list=','.join(str(x) for x in txn_list)
                else:
                    txn_list=''
                print('Fetching Data From %s and To %s' % (acfrm, acto))
                ac_data_set, ac_comp_info = fetch_data(hostip, app_id, acfrm, acto, co_list,txn_list)
                print("Dimension Of The Input Dataset: Rows: %d, Cols: %d and Min Time is %s and Max Time is %s" % (ac_data_set.shape[0], ac_data_set.shape[1], min(ac_data_set.Time),max(ac_data_set.Time)))  # Compare with the corresponding lower and upper boundaries and classify
                if(len(ac_data_set)>0):
                    t1=time.time()
                    hour_data=RD_Convert(ac_data_set,roll_up_size)
                    print("Time Taken To Convert Based On Window Length %s" % (time.time() - t1))
                    t1=time.time()
                    final_result=AD_RT_Wrapper(hour_data,ano_data,bl,app_id,ac_comp_info)
                    final_result = final_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','AD_Method']]
                    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
                    CLS_update_table(conn, final_result, 'ANO')
                    print("Time Taken To Update Anomaly Details Is %s" % (time.time() - t1))
                else:
                    #ano_data['Anomaly'] = 2
                    #CLS_update_table(conn, ano_data,'ANO')
                    print('No Data For The Selected Date Range')
            except:
                print("Please Check Data Service Is Running")
        else:
            print('No Rows In AD_RT_ANOMALYDEF Table is -1')
    except:
        print("Check The DB Connection")
    #conn.close()
    print("Total Time Taken Is %s" % (time.time() - t0))
    print('End Time For Classifier: ' + str(datetime.datetime.now()))
    return final_result

if __name__ == "__main__":
    main()
    AD_RT_Classifier(bl, conn, roll_up_size)