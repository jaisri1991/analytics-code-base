
# Libraries
exec(open(Base_Dir + '\\include\\RT_AD_include.py').read())
# DB Calling Functions
exec(open(Source_Dir + '\\src_utilities\\RT_AD_dbUtilities.py').read())
# DB connections
exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
# Table Transformation function
exec(open(Source_Dir + '\\src_preproc\\RT_AD_fetchRawData.py').read())
# SARIMA function
exec(open(Source_Dir + '\\src_kernel\\RT_AD_sarima.py').read())
# SD Sauare function
exec(open(Source_Dir + '\\src_kernel\\RT_AD_CLF_Moments.py').read())
# Helper function
exec(open(Source_Dir + '\\src_utilities\\RT_AD_helper.py').read())
# Post Process function
exec(open(Source_Dir + '\\src_postproc\\RT_AD_postProc.py').read())
# Correlation Function
exec(open(Source_Dir + '\\src_postproc\\RT_AD_Correlation.py').read())

#Fetching Data Using Data Service
print("Fetching PQD Data")
t1 = time.time()
query = "SELECT * FROM AD_RT_ARIMACONFIG where AppInstanceId=%s" % app_id
bl = db_result(query)
print("Time Taken To Fetch PQD Data: %s" % (time.time() - t1))