import os
# os.chdir('../../')
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '\\src'

exec(open(Base_Dir+'\src\src_kernel\RT_AD_main.py').read())
exec(open(Base_Dir+'\include\RT_AD_Sources.py').read())

def AD_RT_main_scheduler_wrapper():
    print('AD_RT_main_scheduler_wrapper: ' + str(datetime.datetime.now()))
    main()

def AD_RT_classifier_scheduler_wrapper():
    print('AD_RT_classifier_scheduler_wrapper: ' + str(datetime.datetime.now()))
    AD_RT_Classifier(bl,roll_up_size)
    #conn.close


schedule.every(1).hours.do(AD_RT_main_scheduler_wrapper)
schedule.every(20).minutes.do(AD_RT_classifier_scheduler_wrapper)
i=0
while True:
    i=i+1
    print("The Wrapper Run: %s" %i)
    schedule.run_all()
    time.sleep(1)