-- Create table ANOMALYDEF
CREATE TABLE IF NOT EXISTS `AD_RT_ANOMALYDEF` (
  `Time` datetime NOT NULL,
  `App_Id` int(11) NOT NULL,
  `Comp_Id` int(11) NOT NULL,
  `Layer` varchar(20) NOT NULL,
  `Win_Len` int(11) NOT NULL,
  `Comp_Name` varchar(100) NOT NULL,
  `Metric_Name` varchar(100) NOT NULL,
  `Anomaly` int(11) DEFAULT NULL,
  `Mean` float DEFAULT NULL,
  `Lower` float DEFAULT NULL,
  `Upper` float DEFAULT NULL,
  PRIMARY KEY (`Time`,`App_Id`,`Comp_Id`,`Win_Len`,`Comp_Name`,`Metric_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Baseline TABLE
CREATE TABLE IF NOT EXISTS `AD_RT_ARIMACONFIG` (
  `AppInstanceId` bigint(20) DEFAULT NULL,
  `CompInstanceId` bigint(20) DEFAULT NULL,
  `Metric_Name` text,
  `PQD` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create Store Procedure for Corelation KPI Weightage Calcualtion
DROP procedure IF EXISTS `AD_RT_Fetch_Real_Data`;
DELIMITER $$
USE `APPNOMIC`$$
CREATE DEFINER=`root`@`%` PROCEDURE `AD_RT_Fetch_Real_Data`(IN ENAME varchar(50),IN StartDate DateTime,IN EndDate DateTime,IN CompId varchar(50),IN KpiName varchar(1000),IN WIN varchar(5),IN App_Id varchar(4),IN Layer varchar(10))
BEGIN
	Declare SQLQuery varchar(4000);
	SET SESSION group_concat_max_len = 10000;
	SET @query := CONCAT('select "',EndDate,'" - interval 1 Minute into @ed;');
	PREPARE stmt from @query;
	EXECUTE stmt;
	SET EndDate   := @ed;
	DEALLOCATE PREPARE stmt;
	#set @ed=str_to_date('2017-02-14 05:00:00',"%Y-%m-%d %H:%i:%s") - interval 1 Minute;
if (Layer = 'COMP') then
    if (WIN = 'DATE') then
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'SELECT DATE(DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:00:00\")) T,CompInstanceId,\',ClusterOp,\'(\',MetricName,\')\' ,\' Value,\"\',concat(Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',Unit,\'\" AS Unit,\"\',Comp_Name,\'\"AS Comp_Name from \', Colltab,\' where CompInstanceId=\',Comp_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" group by DATE(T)\') query from (select G.Id Comp_Id,G.Name Comp_Name,M.Name MetricName,M.ClusterOp,M.Units Unit,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C,apmcommon.METRICDEF M where G.CompMetricSetId=C.Id and C.Id=M.CompMetricSetId and G.Id in (',CompId,') and M.Name in (',KpiName,')) r) jaya into @jjquery;');

    elseif (WIN ='HOUR') then
		set @sat=concat('ALTER TABLE ',ENAME,'.AD_RT_ANOMALYDEF CHARACTER SET UTF8;');
		prepare st FROM @sat;
		EXECUTE st;
		SET NAMES 'UTF8';
		SET CHARACTER SET 'UTF8';
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'select s.*,a.Anomaly from (SELECT DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:00:00\") T,CompInstanceId,\',ClusterOp,\'(\',MetricName,\')\' ,\' Value,\"\',concat(Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',Unit,\'\" AS Unit,\"\',Comp_Name,\'\"AS Comp_Name from \', Colltab,\' where CompInstanceId=\',Comp_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" group by DATE(T),HOUR(T))s left join AD_RT_ANOMALYDEF a on s.Metric_Name=a.Metric_Name and s.T=a.Time and s.CompInstanceId=a.Comp_Id and a.App_Id=',App_Id,'\') query from (select G.Id Comp_Id,G.Name Comp_Name,M.Name MetricName,M.ClusterOp,M.Units Unit,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C,apmcommon.METRICDEF M where G.CompMetricSetId=C.Id and C.Id=M.CompMetricSetId and G.Id in (',CompId,') and M.Name in (',KpiName,')) r) jaya into @jjquery;');

	else
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'SELECT DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:%i:%s\") T,CompInstanceId,\',MetricName ,\' Value,\"\',concat(Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',Unit,\'\" AS Unit,\"\',Comp_Name,\'\"AS Comp_Name from \', Colltab,\' where CompInstanceId=\',Comp_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" \') query from (select G.Id Comp_Id,G.Name Comp_Name,M.Name MetricName,M.Units Unit,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C,apmcommon.METRICDEF M where G.CompMetricSetId=C.Id and C.Id=M.CompMetricSetId and G.Id in (',CompId,') and M.Name in (',KpiName,')) r) jaya into @jjquery;');
	end if;
else 
    if (WIN = 'DATE') then
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'SELECT DATE(DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:00:00\")) T,TxnId,SUM(\',MetricName ,\') Value,\"\',concat(\'Txn_\',Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',App_Id,\'\" AS App_Id,\"\',Comp_Name,\'\" AS Comp_Name from TRANSACTIONCOLL where TxnId=\',Comp_Id,\' and AppInstanceId=\',App_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" group by DATE(T)\') query from (select distinct Comp_Id,App_Id,Comp_Name,SUBSTRING_INDEX(Metric_Name, \'_\', -1) MetricName from AD_RT_ANOMALYDEF where Metric_Name in (',KpiName,') and App_Id=',App_Id,') r) jaya into @jjquery;');

    elseif (WIN ='HOUR') then
		set @sat=concat('ALTER TABLE ',ENAME,'.AD_RT_ANOMALYDEF CHARACTER SET UTF8;');
		prepare st FROM @sat;
		EXECUTE st;
		SET NAMES 'UTF8';
		SET CHARACTER SET 'UTF8';
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'select s.*,a.Anomaly from (SELECT DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:00:00\") T,TxnId,SUM(\',MetricName ,\') Value,\"\',concat(\'Txn_\',Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',App_Id,\'\" AS App_Id,\"\',Comp_Name,\'\" AS Comp_Name from TRANSACTIONCOLL where TxnId=\',Comp_Id,\' and AppInstanceId=\',App_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" group by DATE(T),HOUR(T)) s left join AD_RT_ANOMALYDEF a on s.Name=a.Metric_Name and s.T=a.Time and s.TxnId=a.Comp_Id and a.App_Id=',App_Id,'\') query from (select distinct Comp_Id,App_Id,Comp_Name,SUBSTRING_INDEX(Metric_Name, \'_\', -1) MetricName from AD_RT_ANOMALYDEF where Metric_Name in (',KpiName,') and App_Id=',App_Id,') r) jaya into @jjquery;');

	else
		Set @SQLQuery = CONCAT('select GROUP_CONCAT(query, \' UNION ALL \' SEPARATOR \' \') FROM (select CONCAT(\'SELECT DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName="',ENAME,'") MICROSECOND),\"%Y-%m-%d %H:%i:%s\") T,TxnId,\',MetricName ,\' Value,\"\',concat(\'Txn_\',Comp_Name,\'_\',MetricName),\'\" Name,\"\',MetricName,\'\" Metric_Name,\"\',App_Id,\'\" AS App_Id,\"\',Comp_Name,\'\" AS Comp_Name from TRANSACTIONCOLL where TxnId=\',Comp_Id,\' and AppInstanceId=\',App_Id,\' and Time between \"',StartDate,'\" and  \"',EndDate,'\" \') query from (select distinct Comp_Id,App_Id,Comp_Name,SUBSTRING_INDEX(Metric_Name, \'_\', -1) MetricName from AD_RT_ANOMALYDEF where Metric_Name in (',KpiName,') and App_Id=',App_Id,') r) jaya into @jjquery;');
    end if;
end if;

    prepare stmt FROM @SQLQuery;
    
    EXECUTE stmt;

Set @subq = @jjquery;
    
if (length(@subq)>10) then
Set @finalq=CONCAT('select substr(@jjquery,1,',length(@subq),'-10)',' into @fq;');
prepare fst FROM @finalq;
execute fst;
        Set @ffq=CONCAT(@fq,'order by T;');
        prepare final from @ffq;
        execute final;
        
else
select NULL;
end if;
END$$

DELIMITER ;