def main():
    # Source Files
    #exce(open('G:\Python\AD_AR_v1.2\include\RT_AD_Sources.py').read())
    # Calling Functions
    t0 = time.time()
    print('Start Time For Main Function: ' + str(datetime.datetime.now()))
    #Fetching Data Using Data Service
    #t1=time.time()
    exec(open(Base_Dir + '\\config\\AD_BL_dbConfig.py').read())
    last_refreshed = calcInputDateRange(conn,app_id)
    frm = (pd.to_datetime(last_refreshed) - datetime.timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
    to = pd.to_datetime(last_refreshed).strftime('%Y-%m-%d %H:%M:%S')

    data_set,comp_info=fetch_data(hostip,app_id,frm,to,comp_list,txn_list)
    print("Time Taken To Fetch KPI Data From DB: %s"%(time.time()-t1))
    print("Dimension Of The Input Dataset: Rows: %d, Cols: %d"%(data_set.shape[0],data_set.shape[1]))
    conn.close

    #Main real time module
    exec(open(Base_Dir + '\\config\\AD_BL_dbConfig.py').read())
    ADR_result = AD_BL_Main(data_set,bl,roll_up_size,app_id,comp_info,nhead)
    print("Dimension Of The Result Dataset: Rows: %d, Cols: %d"%(ADR_result.shape[0],ADR_result.shape[1]))

    conn.close
    print("Total Time Taken: %s"%(time.time()-t0))
    print('End Time For Main Function: ' + str(datetime.datetime.now()))
    return data_set,comp_info,ADR_result#,AD_CL_Result,AD_CO_Result