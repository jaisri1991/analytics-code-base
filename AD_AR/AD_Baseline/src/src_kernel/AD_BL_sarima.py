#@profile(stream=fp)
def AD_BL_Main(data_set,bl,roll_up_size,app_id,comp_info,nhead):

    #Cols for which we need to find out the order
    cols = [col for col in data_set.columns if col not in ['Time','CompInstanceId']]
    data_set[cols] = data_set[cols].apply(pd.to_numeric,errors='coerce')

    #Overlapping average followed by sampling
    #ts_obj = data_set[cols].rolling(roll_up_size).mean()[roll_up_size-1::roll_up_size]
    #Non overlapping average
    ts_obj=non_overlapping_average(data_set,roll_up_size,impute_then_downsample=False)

    #Index with datetime index
    rng = pd.DataFrame(pd.date_range(start = min(ts_obj.Time), periods=ts_obj.shape[0], freq=str(roll_up_size)+'min'),columns=['Time'])
    ts_obj.index = rng['Time']
    ts_obj['Time'] = pd.DatetimeIndex(rng['Time'])

    #Call AD module
    t1=time.time()
    AD_result = AD_BL_SARIMAX_Wrapper(ts_obj,bl,roll_up_size,app_id,comp_info,nhead)
    print("Time Taken To Detect Anomalies Realtime: %s" % (time.time() - t1))

    return AD_result

#@profile(stream=fp)
def AD_BL_SARIMAX_Wrapper(ts_obj,bl,roll_up_size,app_id,comp_info,nhead):

    cols = [col for col in ts_obj.columns if col not in ['Time','CompInstanceId']]
    df_result = pd.DataFrame(columns=['KPI','order','seas_order'])
    for i in cols:
        try:
            df = ts_obj[i]
            if(i[0:3] != 'Txn'):
                comp_id = int(bl[bl.Metric_Name == i].CompInstanceId.item())
                Lay = comp_info[comp_info.CompId == comp_id].Layer.item()
                comp_name = comp_info[comp_info.CompId == comp_id].Name.item()
                met_name = df.name.replace(comp_name + '_', '')
                df.name = met_name
            else:
                comp_name=i.replace('Txn_','').replace('_Total','').replace('_AvgRespTime','').replace('_SlowPercentage','')
                comp_id=comp_info[comp_info.Name == comp_name].CompId.item()
                Lay='TXN'
                met_name=i
            #Using SARIMAX Function
            order, sorder = AD_BL_getOptOrder(df)
            df_result = df_result.append(kpi_result, ignore_index=True)

        except:
             print("Cannot do baseline For %s "%i)
        #Create the result data frame

    result = df_result[['App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'order', 'seas_order']]
    CLS_update_table(conn,result,'BL')
    return result

def AD_BL_getOptOrder(df):

    autoarima_result = sm.tsa.x13.x13_arima_select_order(X_train, maxorder=(2, 1), maxdiff=(2, 1), diff=None, exog=None,
                                               log=None, outlier=True, trading=False, forecast_years=None,
                                               start=None, freq=None, print_stdout=False, x12path=None,
                                               prefer_x13=True)
    return autoarima_result['order'], autoarima_result['sorder']


