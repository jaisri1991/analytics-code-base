def calcInputDateRange(conn,app_id):
    sql_string = 'select max(Time) as Latest_Results_Timestamp, App_Id, Comp_Id, Metric_Name ' \
                 'from AD_RT_ANOMALYDEF where App_Id = ' + str(app_id)

    cursor = conn.cursor()
    cursor.execute(sql_string)
    field_names = [i[0] for i in cursor.description]
    rows = cursor.fetchall()
    if(rows[0][0]!=None):
        time_df = pd.DataFrame(rows,columns = field_names)

        #Last refresh date
        last_refreshed = time_df['Latest_Results_Timestamp'].min().to_pydatetime()
        #run_time_start = time_df['Latest_Results_Timestamp'].min() + datetime.timedelta(hours=1)
        #run_time_end = datetime.datetime.now().replace(minute=0,second=0,microsecond=0)

        #cursor.close()
        #return from_date.strftime("%Y-%m-%d"), to_date.strftime("%Y-%m-%d")
        return last_refreshed
    else:
        return default_to



#Following function converts a n row data frame (with 'Time' as the time stamp in one column) to n//window_len rows of dataframe
#Only averaging is supported for now
#The starting time along with window length (in minutes) ditates the rows of result dataframe
#The window is right adjusted so that the first point in the result data frame comes from (min_time + window_len)th point of the original dataframe
#raw_df is assumed to have minutely data
#impute_then_downsample is a flag which dictates if imputation needs to be done at a minute level before averaging or averaging can be done first
#If averaging is done first then we will need to perform lesser number of imputations
#The sequencing is to be preseved i.e time indices should be row0 - earliest recorded to rown - latest recorded
def non_overlapping_average(raw_df,window_len,impute_then_downsample=False):

    #Get specifics of raw df
    colnames = [col for col in raw_df.columns]
    raw_df_num_rows = raw_df.shape[0]

    #Index with datetime index
    #Min time us taken so that averaging window can be right aligned (similar to real time case where you average backwards)
    #1st hour in dataset and last hour in dataset
    first_hour = min(raw_df.Time).replace(minute=0,second=0) + datetime.timedelta(minutes=60)
    last_hour = max(raw_df.Time).replace(minute=0, second=0) + datetime.timedelta(minutes=60)
    rng = pd.DataFrame(pd.date_range(start = first_hour, end = last_hour, freq=str(window_len)+'min').strftime('%Y-%m-%d %H'),columns=['Time'])
    rng.index = rng['Time']
    #First pass creates index based on downsamples index values (i.e all rows with time stamps
    # between 6:31 and 7:30 are indexed 7:30
    raw_df.index = (raw_df['Time'] + datetime.timedelta(minutes=60)).apply(lambda x: x.strftime('%Y-%m-%d %H'))

    #Group by hour , day, month and year
    averaged_df = raw_df.groupby(raw_df.index).mean()

    #Impute data
    result = rng.join(averaged_df,how='outer')
    cols = [col for col in result.columns if col not in ['Time']]
    #result.fillna(method='ffill',inplace=True)
    result=impute_using_sma(result,cols,5)
	
    return result

#The following function will fill the NaNs with the above row mean values based on the given length column by column
# X is the dataframe which we pass with NaNs
# cols is the column names
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value

def impute_using_sma(x,cols,n):
    if(isinstance(cols,list)):
        for f in cols:
          print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    return x


#The following function will fill the NaNs with the above row mean values based on the given length
# X is the dataframe which we pass with NaNs
# f is the column name
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isnull(x.ix[0,f]) == True):
        x.ix[0, f]=0
    if (count > n):
        ### Method is SMA
        row_no = pd.isnull(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if(len(row_no)==count-1):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()


#@profile(stream=fp)
def data_plot(data_set,pred,kpi,anom_indices):
    mean_pred = pd.Series(pred['Mean'],index=pred.index,name='Mean_Pred')
    plt.plot(mean_pred, label=str(nhead) + '-step ahead Forecast',color='orange',alpha=0.5)

    ax = data_set.plot(label='observed')
    pred_ci = pred[['lower '+kpi,'upper '+kpi]]
    ax.fill_between(pred_ci.index,
                    pred_ci.iloc[:, 0],
                    pred_ci.iloc[:, 1], color='blue', alpha=.1)
    ax.set_xlabel('Date')
    ax.set_ylabel('Metric')
    plt.legend()

    for count in range(len(anom_indices)):
        ax.scatter(data_set[anom_indices].index[count],data_set[anom_indices[count]],color='red',alpha=0.8)

    plt.savefig("output/charts/" + kpi + "_nsteps" + str(nhead) +".png")
    plt.close()


class datetime_mod(datetime.datetime):
    def __divmod__(self, delta):
        seconds = int((self - datetime.datetime.min).total_seconds())
        remainder = datetime.timedelta(
            seconds=seconds % delta.total_seconds(),
            microseconds=self.microsecond,
        )
        quotient = self - remainder
        return quotient, remainder

    def __floordiv__(self, delta):
        return divmod(self, delta)[0]

    def __mod__(self, delta):
        return divmod(self, delta)[1]
