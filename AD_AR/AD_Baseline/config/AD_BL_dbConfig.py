#DB Connection
conn = mysqlcon.connect(
    user='root',
    password='',
    host='10.1.1.202',
    database='APPNOMIC')
engine = create_engine('mysql+mysqlconnector://root:@10.1.1.202:3306/APPNOMIC', echo=False)

#DB variables
hostip='10.1.0.130'
default_frm='2017-03-01'
default_to='2017-03-30'
comp_list='2,122,442,322'
txn_list='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100'
roll_up_size = 60
app_id=2
forecast_steps = 1
nhead = 10

print("Fetching PQD Data")
t1 = time.time()
query = "SELECT * FROM AD_RT_ARIMACONFIG where AppInstanceId=%s" % app_id
bl = db_result(conn, query)
print("Time Taken To Fetch PQD Data: %s" % (time.time() - t1))