# os.chdir('../../')
import os
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '/Src'
Models_Dir = Base_Dir + '/Models'
exec(open(Base_Dir+'/Include/include.py').read())

data_set,comp_info=fetch_data(hostip,65,'2017-08-01','2017-10-31','','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29'
,'Total,AvgRespTime,SlowPercentage')
dd=data_set[:]
#df = non_overlapping_average(dd,1,aggr_method='mean')
ind = DataFrame({'Time':pd.date_range(dd.Time.min(), dd.Time.max(), freq=str(1) + 'min')})
df=ind.merge(dd,on='Time',how='outer')
cols = [col for col in df.columns if col not in ['Time']]
#df=impute_using_sma(df,cols,5)
df=df.fillna(method='ffill')
df=df.fillna(method='bfill')
df.index=df.Time
df.to_csv('UPI_Txn_Data_Aug_To_Oct_2017.csv',index=False)

aggr_method='mean'
win_len=60
percentage=0.8
nhead=5
threshold=2.5
lag=int(1440/win_len)
length=int(lag*7)
elapsed=0
train_test_split=percentage

hour_data = non_overlapping_average(df,win_len,aggr_method)


LR_pipeline=Pipeline([('LR_Transformer',LR_Transformer(win_len=win_len,aggr_method=aggr_method,lag=lag,hour=('09:00','18:00'),length=length,label='1_Week')),('LR_Estimator',Linear_reg_Estimator(cols=list(cols),Classification=True,percentage=percentage,nhead=nhead,aggr_method=aggr_method,elapsed=elapsed,win_len=win_len))])
esti = FeatureUnion([('est1',LR_pipeline),('est2',moment_3_Estimator(percentage=percentage,win_len=win_len,threshold=threshold,aggr_method=aggr_method,elapsed=elapsed)),('est3',moment_4_Estimator(percentage=percentage,win_len=win_len,threshold=threshold,aggr_method=aggr_method,elapsed=elapsed))])
pp=Pipeline([('est',esti),('voting',Voting_Estimator(kpis=list(cols),threshold=2)),('plots',Ploting_Estimator(data_set=hour_data,win_len=win_len,percentage=percentage))])
result=pp.transform(df)


# Arima Code
df=pd.read_csv('G:/Analytics/Customers Files/UPI/Aug_to_Sep.csv')
df.index=pd.to_datetime(df.Time,format='%d-%m-%Y %H:%M')
upi_comp_info=pd.read_csv('G:/Analytics/Customers Files/UPI/comp_info.csv')
kpi_arima_result=SARIMA_FUNC(df.iloc[:,0:2],'kpi',win_len,3,upi_comp_info,nhead,0.999,elapsed)
##Testing
#LR_pipeline=PMMLPipeline([('LR_Transformer',LR_Transformer(win_len=win_len,aggr_method=aggr_method,lag=lag,hour=('09:00','18:00'),length=length,label='1_Week')),('LR_Estimator',Linear_reg_Estimator(cols=list(cols[0:2]),Classification=True,percentage=percentage,nhead=nhead,aggr_method=aggr_method,elapsed=elapsed,win_len=win_len))])
#esti = FeatureUnion([('est1',LR_pipeline),('est2',moment_3_Estimator(percentage=percentage,win_len=win_len,threshold=threshold,aggr_method=aggr_method,elapsed=elapsed)),('est3',moment_4_Estimator(percentage=percentage,win_len=win_len,threshold=threshold,aggr_method=aggr_method,elapsed=elapsed))])
#pp=PMMLPipeline([('est',esti),('voting',Voting_Estimator(kpis=list(cols[0:2]),threshold=2)),('plots',Ploting_Estimator(data_set=hour_data.iloc[:,0:2],win_len=win_len,percentage=percentage))])
#joblib.dump(pp, "pipeline.pkl.z", compress = 9)
#result=pp.transform(df.iloc[:,0:3])
#
#lr=LR_pipeline.transform(df.iloc[:,0:3])
#m3=moment_3(df.iloc[:,0:3],threshold,percentage,win_len,aggr_method,elapsed=elapsed)
#m4=moment_4(df.iloc[:,0:3],threshold,percentage,win_len,aggr_method,elapsed=elapsed)