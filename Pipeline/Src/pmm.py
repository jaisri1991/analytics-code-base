import pandas

iris_df = pandas.read_csv("Iris.csv")

from sklearn_pandas import DataFrameMapper
from sklearn.preprocessing import StandardScaler
from sklearn2pmml.decoration import ContinuousDomain

iris_mapper = DataFrameMapper([
    (["Sepal.Length", "Sepal.Width", "Petal.Length", "Petal.Width"], [ContinuousDomain(), StandardScaler()])
])
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest

iris_pca = PCA(n_components = 3)
iris_selector = SelectKBest(k = 2)
from sklearn.tree import DecisionTreeClassifier

iris_classifier = DecisionTreeClassifier(min_samples_leaf = 5)
from sklearn2pmml import PMMLPipeline

iris_pipeline = PMMLPipeline([
    ("mapper", iris_mapper),
    ("pca", iris_pca),
    ("selector", iris_selector),
    ("estimator", iris_classifier)
])
iris_pipeline.fit(iris_df, iris_df["Species"])
from sklearn.externals import joblib

joblib.dump(iris_pipeline, "pipeline.pkl.z", compress = 9)