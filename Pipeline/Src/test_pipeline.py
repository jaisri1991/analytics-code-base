from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.ensemble import RandomForestClassifier, VotingClassifier

class TemplateEstimator1(BaseEstimator, TransformerMixin):
    
    def __init__(self, demo_param='demo_param'):
        self.demo_param = demo_param


#    def predict(self, X):
#        print('estimator1 predict')
#        return self
    
    def transform(self,final):
        print('estimator1 transfrom')
        kks=list(final.keys())
        for k in kks:
            full_df=final[k]
            r=DataFrame({k:full_df.sum(axis=1)})
            r=r/2
            for i in range(0,len(r)):
                if(r.iloc[i].values[0]>5):
                    r.iloc[i]=int(1)
                else:
                    r.iloc[i]=int(0)
            if(k==kks[0]):
                result=r
            else:
                result=result.join(r,lsuffix=True)
        return result
    
    def fit(self,final):
        print('estimator1 fit')
        return final
        
    
class TemplateEstimator2(BaseEstimator, TransformerMixin):
    
    def __init__(self, demo_param='demo_param'):
        self.demo_param = demo_param

#    def predict(self, X):
#        print('estimator2 predict')
#        return self
    
    def transform(self, final):
        print('estimator2 transfrom')
        kks=list(final.keys())
        for k in kks:
            full_df=final[k]
            r=DataFrame({k:full_df.sum(axis=1)})
            for i in range(0,len(r)):
                if(r.iloc[i].values[0]>5):
                    r.iloc[i]=int(1)
                else:
                    r.iloc[i]=int(0)
            if(k==kks[0]):
                result=r
            else:
                result=result.join(r,lsuffix=True)
        return result

    def fit(self,final):
        print('estimator2 fit')
        return final
        
    
class TemplateTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, demo_param='demo'):
        self.demo_param = demo_param


    def transform(self, X):
        print('Transformer transfrom')        
        final={}
        cols=X.columns
        for co in cols:
            df=DataFrame(X[co])
            df[co+'_by_2']=df[co]/2            
            final[co]=DataFrame(df)
        return final
    
    def fit(self,final):
        print('Transformer Fit')
        return final
    
class TemplateVoting(BaseEstimator, TransformerMixin):
    def __init__(self, kpis,threshold=1):
        self.kpis = kpis
        self.threshold = threshold

    def fit(self,result):
        print('Voting Fit')
        return result

    def transform(self, result):
        print('Voting transfrom')   
        ff=DataFrame()
        rr=DataFrame(result,columns=np.concatenate([np.array(i) for i in [self.kpis]*round(len(result[0])/len(self.kpis))]))
        for i in self.kpis:
            df=rr[i].astype(int)
            sum_df=DataFrame(df.sum(axis=1),columns=[i])
            index=sum_df[(sum_df[i]>self.threshold)].index
            sum_df[i]=0
            sum_df[i][index]=1
            if(i==self.kpis[0]):
                ff=sum_df
            else:
                ff=ff.join(sum_df,lsuffix=True)
        return ff


X={'kpi1':range(0,10),'kpi2':range(10,20)}
X=DataFrame(X)
esti = FeatureUnion([('est1',TemplateEstimator1()),('est2',TemplateEstimator2())])
pp=Pipeline([('trans',TemplateTransformer()),('est',esti),('voting',TemplateVoting(kpis=list(X.columns),threshold=1))])
rr=pp.transform(X)
rr