def plot_java(percentage,win_len):

    files=os.listdir('Data')
    #files.remove('RIB_Data_Jan_Month.csv')
    #dd=pd.read_csv('Data/RIB_Data_Jan_Month.csv')
    #dd.columns=dd.columns.str.strip()
    #dd.Time=dd.Time.str.strip()
    #dd.Time=pd.to_datetime(dd.Time)#,format='%d-%m-%Y %H:%M')
    #ind = DataFrame({'Time':pd.date_range(dd.Time.min(), dd.Time.max(), freq=str(1) + 'min')})
    #dd=ind.merge(dd,on='Time',how='outer')
    #dd=dd.fillna(method='ffill')
    #dd=dd.fillna(method='bfill')
    #dd.index=dd.Time
    ano_details=pd.DataFrame({'KPI':[0]*len(files),'Arima_Mean':0,'Nrow':0})
    j=0
    #df=non_overlapping_average(dd,win_len,'mean')
    for file in files:
        kpi=file.replace(' .csv','')
        data=pd.read_csv('Data/'+file)
        data.columns=data.columns.str.strip()
        data.rename(columns={'Is Anomalies':kpi}, inplace=True)
        c = [col for col in data.columns if col not in ['Time']]
        data[c]=data[c].apply(pd.to_numeric, errors='coerce')
        if not os.path.exists('Plots/Java'):
            os.makedirs('Plots/Java')
        os.chdir('Plots/Java')
        data.index=pd.to_datetime(data.Time,format='%d-%m-%Y %H:%M')
        #axvl=data.index[0]
        axvl=data[round(len(data)*percentage):].index[0]
        plt.figure(figsize=(12,10))
        #plt.plot(df[kpi],color='orange')#[df.index[0]:data.index[len(data)-1]])
        plt.plot(data['Actual'],color='orange')
        plt.plot(data['Forecasts'],'--',color='b')
        high=data[(data[kpi]>0)].index
        #plt.plot(df[kpi][high],'ro',color='r')
        plt.plot(data['Actual'][high],'ro',color='r')
        plt.axvline(x=axvl,color='r')
        plt.xticks(rotation=45)
        plt.title(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc.jpg')
        ano_details.loc[j,'KPI']=kpi
        ano_details.loc[j,'Arima_Mean']=len(high)
        ano_details.loc[j,'Nrow']=len(data[(data['Forecasts']>0)])
        j=j+1
        os.chdir('../..')
    ano_details.to_csv('ano_details.csv')
