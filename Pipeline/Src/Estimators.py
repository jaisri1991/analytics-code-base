class Linear_reg_Estimator(TransformerMixin):
    
    def __init__(self,cols,Classification=True,percentage=0.8,nhead=1,aggr_method='mean',elapsed=0,win_len=60):
        self.cols=cols
        self.Classification=Classification
        self.percentage=percentage
        self.nhead=nhead
        self.aggr_method=aggr_method
        self.win_len=win_len
        self.elapsed=elapsed
   
    def transform(self,result):
        print('Linear Regression Estimator for '+str(self.win_len))        
        output=Linear_reg(result,self.cols,self.Classification,self.percentage,self.nhead,self.elapsed,self.win_len)
        return output
    
    def fit(self, X, y=None):
        return self
    
class moment_3_Estimator(TransformerMixin):
    
    def __init__(self,percentage=0.8,win_len=60,threshold=2.5,aggr_method='mean',elapsed=0):
        self.percentage=percentage
        self.win_len=win_len
        self.threshold=threshold
        self.aggr_method=aggr_method
        self.elapsed=elapsed
   
    def transform(self,data_set):
        print('Moment3 Estimator for '+str(self.win_len))
        final_output=moment_3(data_set,self.threshold,self.percentage,self.win_len,self.aggr_method,self.elapsed)
        return final_output
    
    def fit(self, X, y=None):
        return self
    
class moment_4_Estimator(TransformerMixin):
    
    def __init__(self,percentage=0.8,win_len=60,threshold=2.5,aggr_method='mean',elapsed=0):
        self.percentage=percentage
        self.win_len=win_len
        self.threshold=threshold
        self.aggr_method=aggr_method
        self.elapsed=elapsed
   
    def transform(self,data_set):
        print('Moment4 Estimator for '+str(self.win_len))
        final_output=moment_4(data_set,self.threshold,self.percentage,self.win_len,self.aggr_method,self.elapsed)
        return final_output
    
    def fit(self, X, y=None):
        return self


class Voting_Estimator(BaseEstimator):
    def __init__(self,kpis,threshold=1):
        self.kpis = kpis
        self.threshold = threshold

    def fit(self,result):
        return result

    def transform(self, result):
        print('Majority Voting Estimator')
        max_vote=DataFrame()
        estimator_output=DataFrame(result,columns=np.concatenate([np.array(i) for i in [self.kpis]*round(len(result[0])/len(self.kpis))]))
        for i in self.kpis:
            df=estimator_output[i].astype(int)
            sum_df=DataFrame(df.sum(axis=1),columns=[i])
            #index=sum_df[(sum_df[i]>self.threshold)].index
            #sum_df[i]=0
            #sum_df[i][index]=1
            if(i==self.kpis[0]):
                max_vote=sum_df
            else:
                max_vote=max_vote.join(sum_df,lsuffix=True)
        return max_vote 

class Ploting_Estimator(BaseEstimator):
    def __init__(self,data_set,percentage=0.8,win_len=60):
        self.data_set = data_set
        self.percentage = percentage
        self.win_len=win_len

    def fit(self,result):
        return result

    def transform(self, result):
        print('Ploting Estimator for '+str(self.win_len))
        plot_data(result,self.data_set,self.percentage,self.win_len)