class LR_Transformer(TransformerMixin):
    
    def __init__(self,win_len=60,aggr_method='mean',lag=24,hour=('09:00','18:00'),length=168,label='1_Week'):
        self.win_len=win_len
        self.aggr_method=aggr_method
        self.lag=lag
        self.hour=hour
        self.length=length
        self.label=label
        
    def transform(self,data_set):
        cols = [col for col in data_set.columns if col not in ['Time']]
        result={}
        print('Window Length:'+str(self.win_len))
        for col in cols:
            print('Transformation for '+col)
            df=DataFrame({'Time':data_set.Time,'Actual':data_set[col]})
            hour_data=non_overlapping_average(df,self.win_len,self.aggr_method)
            df1=DataFrame({'Actual':hour_data.Actual})
            df2=df1[:]            
            seq_lag=features.Sequential_Lag(df2,self.lag)
            seas_lag=features.Seasonal_Lag(df2,self.length,self.label)
            tf=features.Time_Features(df2)
            bh=features.Business_Hour(df2,self.hour)
            wday_wend=features.Weekday_or_WeekEnd(df2)
            df2['Time']=df2.index
            for edf in [seq_lag,seas_lag,tf,bh,wday_wend]:
                df2=df2.merge(edf,on=['Time','Actual'],how='outer')
            df2=df2.dropna()
            df2.index=df2.Time
            df2.drop('Time', axis=1, inplace=True)
            result[col]=DataFrame(df2)
        return result
    
    def fit(self, *_):
        return self
