def SARIMA_FUNC(data_set,name,bl,win_len,app_id,comp_info,nhead,train_test_split,elapsed=0,stl=False,org_data=None):
        
    if(elapsed==1):
        f=open('Logs/Arima_output_time_time_'+str(win_len)+'_Win_len and '+str(train_test_split)+'_Percentage'+name+'.log','w')
        t0=time.time()
    else:
        f=open('Logs/Arima_output_time_processtime_'+str(win_len)+'_Win_len and '+str(train_test_split)+'_Percentage'+name+'.log','w')
        t0=time.process_time()
    print('Start Time : ' + str(datetime.datetime.now()),file=f)
    data=data_set[:]
    cols = [col for col in data.columns if col not in ['Time','CompInstanceId']]
    ano_details=DataFrame({'KPI':cols,'Arima_Mean':0,'Nrow':0})
    if(win_len!=1):
        ts_obj=non_overlapping_average(data,win_len,'mean')
    else:
        ts_obj=data[:]
    #rng = pd.DataFrame(pd.date_range(start = min(ts_obj.index), periods=ts_obj.shape[0], freq=str(win_len)+'min'),columns=['Time'])
    ts_obj.index = ts_obj['Time']
    #ts_obj['Time'] = pd.DatetimeIndex(rng['Time'])
    
    df_result = pd.DataFrame()
    #query = "SELECT * FROM AD_RT_ARIMACONFIG where AppInstanceId=%s" % app_id
    #bl = db_result(query)
    j=0
    for i in cols:
        try:
            if(elapsed==1):
                t1=time.time()
            else:
                t1=time.process_time()
            pqd = bl[bl.Metric_Name == i].PQD.item()
            pqd=pqd.split(',')
            pqd=pd.to_numeric(pqd)
            pqd=tuple(pqd)
            df = ts_obj[i]
            if(i[0:3] != 'Txn'):
                comp_id = int(bl[bl.Metric_Name == i].CompInstanceId.item())
                Lay = comp_info[comp_info.CompId == comp_id].Layer.item()
                comp_name = comp_info[comp_info.CompId == comp_id].Name.item()
                met_name = df.name.replace(comp_name + '_', '')
                df.name = met_name
            else:
                comp_name=i.replace('Txn_','').replace('_Total','').replace('_AvgRespTime','').replace('_SlowPercentage','')
                comp_id=comp_info[comp_info.Name == comp_name].CompId.item()
                Lay='TXN'
                met_name=i
            #Using SARIMAX Function
            try:
                if(elapsed==1):
                    t2=time.time()
                else:
                    t2=time.process_time()
                pred = RT_forecast(df,nhead,pqd,train_test_split)
                count = len(pred)
                if(i==cols[0]):
                    print('Total Prediction Points:'+str(count),file=f)
                kpi = pd.DataFrame({'Time': pred.index, 'Win_Len': [win_len] * count, 'App_Id': [app_id] * count,
                                    'Comp_Id': [comp_id] * count, 'Comp_Name': [comp_name] * count,
                                    'Layer': [Lay] * count,'Metric_Name': [met_name] * count, 'Anomaly': -1})                
                kpi_range = pd.DataFrame(
                    {'Time': pred.index, 'Mean': pred.Mean, 'Lower': pred['lower ' + met_name],
                     'Upper': pred['upper ' + met_name],'Actual_Value':df.iloc[-count:]})
                kpi_result = pd.merge(kpi, kpi_range, on='Time', how='outer')
                kpi_result.index=kpi_result.Time
                anom_indices = kpi_result[(kpi_result['Actual_Value'] < kpi_result['Lower']) |
                                (kpi_result['Actual_Value'] > kpi_result['Upper'])].index
                kpi_result['Anomaly']=0
                kpi_result['Anomaly'][anom_indices] = 1
                if(elapsed==1):
                    print(i+' Prediction Time:'+str(time.time()-t2)+',Average Time:'+str((time.time()-t2)/count),file=f)
                    print('Total Time for '+i+' '+str(time.time()-t1),file=f)
                
                else:
                    print(i+' Prediction Time:'+str(time.process_time()-t2)+',Average Time:'+str((time.process_time()-t2)/count),file=f)
                    print('Total Time for '+i+' '+str(time.process_time()-t1),file=f)
                df_result = df_result.append(kpi_result,ignore_index=True)
                ano_details.loc[j,'Arima_Mean']=len(anom_indices)
                ano_details.loc[j,'Nrow']=len(df)
                j=j+1
                if(stl==True):
                    #org_data.index=org_data.Time
                    #org_data=non_overlapping_average(data,win_len,'mean')
                    plt.figure(figsize=(12,10))
                    plt.plot(org_data[i])
                    plt.plot(org_data[i][anom_indices],'ro')
                    plt.axvline(x=str(df_result.Time[0]),color='r')
                    plt.title('Arima_'+str(i)+' With '+str(win_len)+'_Win_len and '+str(train_test_split)+'_Perc')
                    plt.savefig('Plots/Arima/Arima_'+str(i)+' With '+str(win_len)+'_Win_len and '+str(train_test_split)+'_Perc.jpg')
                else:                    
                    plt.figure(figsize=(12,10))
                    plt.plot(df)
                    plt.plot(df[anom_indices],'ro')
                    plt.axvline(x=str(df_result.Time[0]),color='r')
                    plt.title('Arima_'+str(i)+' With '+str(win_len)+'_Win_len and '+str(train_test_split)+'_Perc')
                    plt.savefig('Plots/Arima/Arima_'+str(i)+' With '+str(win_len)+'_Win_len and '+str(train_test_split)+'_Perc.jpg')
                    
            except:
                print('Cannot Do Realtime For %s, The PQD Value is %s'%(i,pqd))

        except:
             print("Cannot Do Realtime For %s Since There Is No PQD Data Available In DB"%i)
        #Create the result data frame

    if(len(result)>0):
        result = df_result[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value']]
    #exec(open(Base_Dir + '\\config\\RT_AD_dbConfig.py').read())
    else:
        result=DataFrame()
    if(elapsed==1):
        print('Total Time Taken: ' + str(time.time()-t0),file=f)
    else:
        print('Total Time Taken: ' + str(time.process_time()-t0),file=f)
    print('End Time : ' + str(datetime.datetime.now()),file=f) 
    print(ano_details,file=f)
    f.close()    
    return result