def Linear_reg(data_set,cols,Classification=True,percentage=0.8,nhead=1,elapsed=0,win_len=60):

    if(elapsed==1):
        f=open('Logs/LR_output_time_time_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.time()
    else:
        f=open('Logs/LR_output_time_processtime_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.process_time()
    print('Start Time : ' + str(datetime.datetime.now()),file=f)
    # Split the data into training/testing sets
    #cols=list(data_set.keys())
    final_output=DataFrame()
    for col_name in cols: 
        if(elapsed==1):
            t1=time.time()
        else:
            t1=time.process_time()
        print(col_name)
        
        full_df=data_set[col_name]
        Train = full_df[:(len(full_df)*percentage).__round__()]
        Test = full_df[(len(full_df)*percentage).__round__():]
        final=[]
        avg=[]
        for i in range((len(Test) / nhead).__round__()+1):
            if(elapsed==1):
                t2=time.time()
            else:
                t2=time.process_time()
            if (i == 0):
                X_Train = Train
                X_Test = Test[0:nhead]
                Y_Train = Train.Actual
                #Y_Test = Test[0:5].Actual.reshape(-1,1)
            else:
                X_Train=full_df[(i*nhead):(len(Train)+(i*nhead))]
                X_Test=Test[i*nhead:(i+1)*nhead]
                Y_Train=full_df.Actual[(i*nhead):(len(Train)+(i*nhead))]
            lr_cols = [col for col in full_df.columns if col not in ['Actual']]               
            model=sm.OLS(Y_Train,X_Train[lr_cols])
            mfit=model.fit()
            Y_Pred=np.array(mfit.predict(X_Test[lr_cols]))
            if(len(Y_Pred)>0):
                final.append(Y_Pred)
            if(elapsed==1):
                avg.append(time.time()-t2)
            else:
                avg.append(time.process_time()-t2)
        print(col_name+' Linear_Regression Prediction '+'Total Time:'+str(sum(avg))+',Average Time:'+str(st.mean(avg)),file=f)
        lr_result=np.concatenate([np.array(i) for i in final])
        Actual=Test.Actual
        #print('Classification : '+str(Classification))
        if(Classification==True):
            error=np.array(Actual-lr_result)
            n=len(Actual)
            k=len(lr_cols)
            sums= (np.square(error)).sum()/(n-k-1)
            se=np.sqrt(sums)*2.5
            output=DataFrame({'Time':Test.index,'Metric_Name':col_name,'Actual':Actual,'Predicted':lr_result,'Lower':lr_result-se,'Upper':lr_result+se,'Anomaly':0})
            indexes=output[(output['Actual'] < output['Lower']) | (output['Actual'] > output['Upper'])].index
            output['Anomaly'][indexes]=1
        else:
            output=DataFrame({'Time':Test.index,'Metric_Name':col_name,'Actual':Actual,'Predicted':lr_result})
        final_output[col_name]=DataFrame(list(output.Anomaly),columns=[col_name])
        if(elapsed==1):
            print('Time Taken For: '+col_name+' is '+ str(time.time()-t1),file=f)
        else:
            print('Time Taken For: '+col_name+' is '+ str(time.process_time()-t1),file=f)
        
    
    final_output.index=output.Time
    if(elapsed==1):
        print('Total Time Taken: ' + str(time.time()-t0),file=f)
    else:
        print('Total Time Taken: ' + str(time.process_time()-t0),file=f)
    print('Linear Regression Output DataFrame Dimension : ' + str(final_output.shape),file=f)
    print('End Time : ' + str(datetime.datetime.now()),file=f)
    f.close()
    return final_output

