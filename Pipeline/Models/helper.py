def plot_data(peak_data,hour_data,percentage,win_len):
    cols = [col for col in hour_data.columns if col not in ['Time']]
    peak_data.index=hour_data[round(len(hour_data)*percentage):].index
    if not os.path.exists('Plots/Voting_Majority'):
        os.makedirs('Plots/Voting_Majority')
    os.chdir('Plots/Voting_Majority')
    for col in cols:
        plt.figure(figsize=(12,10))
        plt.plot(hour_data[col]) 
        high=peak_data[(peak_data[col]>2)].index
        med=peak_data[(peak_data[col]>1)].index
        low=peak_data[(peak_data[col]>0)].index
        plt.plot(hour_data[col][low],'ro',color='yellow')
        plt.plot(hour_data[col][med],'ro',color='orange')
        plt.plot(hour_data[col][high],'ro',color='r')
        plt.axvline(x=str(peak_data.index[0]),color='r')
        plt.title(str(col)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(col)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc.jpg')
    os.chdir('../..')
    
def db_result(query):
    conn=dbfun()
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        df=pd.DataFrame(rows)
        if(len(df)>0):
            df.columns=[i[0] for i in cursor.description]
            print('Total Row(s):', cursor.rowcount)
        return df
    except Error as e:
        print(e)
    finally:
        cursor.close()
        
#@profile(stream=fp)
def fetch_data(hostip,app_id,frm,to,comp_list,txn_list,txn_kpis='Total,AvgRespTime,SlowPercentage'):
    t0=time.time()
    t1 = time.time()
    dquery = ('select G.Id CompId,G.Name Name,C.CollTableName Colltab from GENCOMPONENTDEF G,apmcommon.COMPMETRICSETDEF C where G.CompMetricSetId=C.Id and G.Id in (%s);' %comp_list)
    data_refresh = DataFrame()
    com_data=DataFrame()
    try:
        if(len(comp_list)>0):
            com_data = db_result(dquery)
            com_data['Layer']=len(com_data) * ['NA']
        else:
                print('No Comp Id Given')
        try:
            if(len(txn_list)>0):
                tquery = ('select distinct TxnId CompId,Name from TRANSACTIONCOLL where AppInstanceId=%s and Time between \'%s\' and \'%s\' and TxnId in (%s) order by Name'%(app_id,frm,to,txn_list))
                tco_data = db_result(tquery)
                if(len(tco_data)>0):
                    tco_data['Colltab']='TRANSACTIONCOLL'
                    tco_data['Layer']='TXN'
                    com_data=com_data.append(tco_data,ignore_index=True)
                    com_data['App_Id'] = app_id
                    print('Started Collecting Transaction Data')
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip, frm, to,app_id,'Total,AvgRespTime,SlowPercentage',txn_list)) as url:
                    #   txn_data = json.loads(url.read().decode())
                    r = requests.get("http://%s:4567/v1.0/app/txn/%s/%s/%s/%s/%s" % (hostip, frm, to,app_id,txn_kpis,txn_list))
                    txn_data=r.json()
                    if ((txn_data['responseStatus'] in ('Sucess', 'success')) & (isinstance(txn_data['result'], list)) & (len(txn_data['result'])>1)):
                        txn_data = DataFrame(txn_data['result'])
                        #txn_data = txn_data[:-1]
                        txn_data['Time'] = pd.to_datetime(txn_data['Time'], format='%Y-%m-%d %H:%M:%S')
                        #txn_data['Time']=pd.to_datetime((txn_data['Time']+ datetime.timedelta(hours=5, minutes=30)).apply(lambda x: x.strftime('%Y-%m-%d %H:%M:%S')),format='%Y-%m-%d %H:%M:%S')
                        print('Transaction Data Set %s,%s'%(txn_data.shape[0],txn_data.shape[1]))
                        data_refresh = txn_data
                        print('Completed Transaction Data')
                    else:
                        print('No Txn Data For the selected Date range')
                else:
                    print('No Txn Data For the selected Date range')
            else:
                print('No Txn Id Given')
            Layer = com_data
            if (len(comp_list) > 0):
                for i in com_data[(com_data.Layer !='TXN')].CompId:
                    colltab = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Colltab.item()
                    comp_name = com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item()
                    print('Started Collecting Data For %s' % comp_name)
                    #with urllib.request.urlopen("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab)) as url:
                    #    kpidata = json.loads(url.read().decode())
                    r=requests.get("http://%s:4567/v1.0/app/compInstance/%s/%s/%d/%s" % (hostip, frm, to, i, colltab))
                    kpidata=r.json()
                    if ((kpidata['responseStatus'] in ('Sucess', 'success')) & (isinstance(kpidata['result'], list)) & (len(kpidata['result'])>1)):
                        data = DataFrame(kpidata['result'])
                        #data = data[:-1]
                        print('%s Data Set %s,%s' % (comp_name,data.shape[0], data.shape[1]))
                        data['Time'] = pd.to_datetime(data['Time'], format='%Y-%m-%d %H:%M:%S')
                        Layer.set_value(Layer[(Layer.CompId == i)&(Layer.Layer !='TXN')].CompId.index.item(), 'Layer',
                                        re.sub(' ', '_', kpidata['properties']['layerName']))
                        col = [col for col in data.columns if
                               col not in ['MININGCLUSTERID', 'BMININGCLUSTERID', 'TRANSACTION_CLUSTER_ID',
                                           'COMPINSTANCEID','Time']]
                        col.extend(['Time'])
                        data = data[col]
                        cols=[com_data[(com_data.CompId == i)&(com_data.Layer !='TXN')].Name.item() + '_' + str(j) for j in data.iloc[:, data.columns != 'Time'].columns]
                        cols.extend(['Time'])
                        data.columns=cols
                        #if ((i == com_data.CompId[0]) & (txn_list=='NULL')):
                        if(len(data_refresh)==0):
                            data_refresh = data_refresh.append(data)
                        else:
                            data_refresh = pd.merge(data_refresh, data, on='Time', how='outer')
                        print('Collected Data For %s' % comp_name)
                    else:
                        print('No data for the selected date range')
            if(len(data_refresh)>0):
                print("Time Taken To Fetch Data: %s" % (time.time() - t1))
                t1=time.time()
                #com_data=pd.merge(com_data,Layer,how='outer',on=
                com_data=Layer
                #print("Filling Missing Values")
                #rng = pd.date_range(start=min(data_refresh.Time), periods=data_refresh.shape[0], freq=str(1) + 'min')
                #rng = pd.DataFrame(rng, columns=['Time'])
                cols = [col for col in data_refresh.columns if col not in ['Time']]
                data_refresh['Time'] = pd.DatetimeIndex(data_refresh['Time'])
                data_refresh[cols] = data_refresh[cols].apply(pd.to_numeric, errors='coerce')
                #complete_data = pd.merge(data_refresh, rng, how='outer', on=['Time'])
                #complete_data=do(complete_data,cols,5)
                #print("Time Taken To Fill Missing Values: %s" % (time.time() - t1))
        except:
            print('Check if the DataService is running')
    except:
        print('DB Connection Failed')
    print("Total Time Taken To Fetch KPI Data From DB: %s" % (time.time() - t0))
    return data_refresh,com_data
    
#Following function converts a n row data frame (with 'Time' as the time stamp in one column) to n//window_len rows of dataframe
#Only averaging is supported for now
#The starting time along with window length (in minutes) ditates the rows of result dataframe
#The window is right adjusted so that the first point in the result data frame comes from (min_time + window_len)th point of the original dataframe
#raw_df is assumed to have minutely data
#impute_then_downsample is a flag which dictates if imputation needs to be done at a minute level before averaging or averaging can be done first
#If averaging is done first then we will need to perform lesser number of imputations
#The sequencing is to be preseved i.e time indices should be row0 - earliest recorded to rown - latest recorded
#@profile
def non_overlapping_average(raw_df,window_len,aggr_method='mean'):
    averaged_df=DataFrame()
    if((isinstance(raw_df.index,pd.core.indexes.datetimes.DatetimeIndex))):
        if (aggr_method == 'mean'):
            averaged_df = raw_df.resample(str(window_len)+'Min').mean()
        elif (aggr_method == 'median'):
            averaged_df = raw_df.resample(str(window_len)+'Min').median()
        elif (aggr_method == 'skew'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.skew)
        elif (aggr_method == 'kurt'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.kurt)
    else:
        print('DataFrame Index is not DateTime')
    return averaged_df

#The following function will fill the NaNs with the above row mean values based on the given length column by column
# X is the dataframe which we pass with NaNs
# cols is the column names
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value
#@profile
def impute_using_sma(x,cols,n):
    #t1 = time.time()
    #print('Imputation Process Started')
    if(isinstance(cols,list)):
        for f in cols:
          #print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    #print("Total Time Taken For Imputation Process: %s" % (time.time() - t1))
    return x


#The following function will fill the NaNs with the above row mean values based on the given length
# X is the dataframe which we pass with NaNs
# f is the column name
# n is the length, whether the function should take above 5 data points or 10 data points for the avearage value

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isnull(x.ix[0,f]) == True):
        nn = x[f].notnull().nonzero()[0]
        if (len(nn) > 1):
            x.ix[0, f]=x.ix[nn[0]:nn[1]+1, f].mean()
        else:
            x.ix[0, f] =0
    if (count > n):
        ### Method is SMA
        row_no = pd.isnull(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if((len(row_no)==count-1)&(x.ix[0, f]==0)):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()


def RT_forecast(df,nhead,pqd,train_test_split = 0.8):
    X_train=df[:(len(df)*train_test_split).__round__()]
    X_test=df[(len(df)*train_test_split).__round__():]
    test_result=pd.DataFrame()
    obs = np.zeros(nhead)
    if(len(X_test) == 0):
        results = SARIMAX_fit(X_train, pqd)
        obs = np.zeros(nhead)
        pred = SARIMAX_forecast(results, nhead)
        pred_mean=pd.DataFrame({'obs_'+df.name:obs,'Mean':pred.predicted_mean})
        dff=pd.concat([pred_mean,pred.conf_int(alpha=0.003)],axis=1)
        test_result=test_result.append(dff)
    else:
        for i in range(0,(len(X_test)/nhead).__round__()):
            print("Running detection for kpi: " + df.name)
            if(i==0):
                results = SARIMAX_fit(X_train,pqd)
            else:
                data = df[(i*nhead):(len(X_train)+(i*nhead))]
                results = SARIMAX_fit(data, pqd)
                obs = X_test[i*nhead:(i+1)*nhead]
            gc.collect()
            pred = SARIMAX_forecast(results, nhead)
            print(str(i) + ' Done' + ' Out Of ' + str((len(X_test) / nhead).__round__()))

            pred_mean=pd.DataFrame({'obs_'+df.name:obs,'Mean':pred.predicted_mean})
            dff=pd.concat([pred_mean,pred.conf_int(alpha=0.003)],axis=1)
            test_result=test_result.append(dff)
    test_result=test_result[test_result.index<=X_test.index[-1]]
    return test_result

def SARIMAX_fit(ts_obj,pqd):
    if (len(pqd) > 3):
        mod = sm.tsa.statespace.SARIMAX(ts_obj,
                                        order=(int(pqd[0]),int(pqd[1]),int(pqd[2])),
                                        seasonal_order=(int(pqd[3]),int(pqd[4]),int(pqd[5]),int(pqd[6])),
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)
    else:
        mod = sm.tsa.statespace.SARIMAX(ts_obj,
                                        order=(int(pqd[0]),int(pqd[1]),int(pqd[2])),
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)

    results = mod.fit(maxiter=20,disp=0)
    gc.collect()

    return results

def SARIMAX_forecast(results,nhead):
    pred=results.get_forecast(nhead)
    gc.collect()
    return pred

