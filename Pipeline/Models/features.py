class features():
    def Sequential_Lag(df,lag=24):
        df1=df[:]
        df2=DataFrame()
        if((len(df1.keys())==1) & (isinstance(df1.index,pd.core.indexes.datetimes.DatetimeIndex))):
            for i in range(1,int(lag)+1):
                    df1['shift%s'%i]=df1[[df1.keys()[0]]].shift(i)
            #df2=df1.iloc[int(lag):]
            df2=df1.fillna(0)
            df2['Time']=df2.index
        else:
            print('Data Frame Has More than One Column OR The Index Is Not In Proper Format(DatetimeIndex)')
        return df2
    
    def Seasonal_Lag(df,length=168,label='1_Week'):
        df1=df[:]
        df2=DataFrame()
        if((len(df1.keys())==1) & (isinstance(df1.index,pd.core.indexes.datetimes.DatetimeIndex))):
            
            if((len(df1)>=length) & (length>0)):
                df1['Same_Hour_'+label+'_Ago']=df1[[df1.keys()[0]]].shift(length)
                #df2=df1.iloc[168:]
                df2=df1.fillna(0)
            #elif(len(df1)>=1440):
            #    df1['Same_Hour_1_Month_Ago']=df1[[df1.keys()[0]]].shift(720)  
            #    df2=df1.iloc[720:]
                df2['Time']=df2.index
        else:
            print('Data Frame Has More than One Column OR The Index Is Not In Proper Format(DatetimeIndex)')
        return df2
    
    def Time_Features(df):
        df2=df[:]
        df2['Hour_Of_Day']=np.array(([int(df2.index[i].strftime("%H")) for i in range(len(df2))]))
        df2['Day_Of_Week']=np.array(([int(df2.index[i].strftime("%d")) for i in range(len(df2))])) 
        df2['Month_Of_Year']=np.array(([int(df2.index[i].strftime("%m")) for i in range(len(df2))]))
        df2['Quarter_Of_Year'] = np.array(df2.index.quarter)
        df2['Time']=df2.index        
        return df2
    
    def Business_Hour(df,hour=('09:00','18:00')):
        df2=df[:]
        if(len(hour)==2):        
            df2['Business_Hour']=0
            index=df2.between_time(hour[0],hour[1]).index
            df2['Business_Hour'][index]=1
            df2['Time']=df2.index
        else:
            print('Please provide Two Elements For Hour Parameter(eg: hour(\'09:00\',\'18:00\'))')
        return df2
    
    def Weekday_or_WeekEnd(df):
        df2=df[:]
        df2['Weekno']=[df2.index[x].weekday() for x in range(0,len(df2))]
        wday=df2[(df2['Weekno'] >= 0) & (df2['Weekno'] < 5)].index
        df2['Weekno']=0
        df2['Weekno'][wday]=1
        df2['Time']=df2.index
        return df2

    def Daily_Average(df):
        uniq_dates=np.unique(df.index.strftime('%Y-%m-%d'))
        df2 = df[:]
        df2['Daily_Average']=0
        frm=uniq_dates[0]
        for i in uniq_dates[7:]:
            rng=df2[(df2.index >= frm + ' 00:00:00') & (df2.index < i + ' 00:00:00')]
            day=pd.to_datetime(i).weekday()
            avg_value=rng[rng.columns[0]][rng.index.weekday==day].mean()
            df2['Daily_Average'][(df2.index >= i + ' 00:00:00') & (df2.index <= i + ' 23:00:00')]=avg_value
        df2['Time'] = df2.index
        return df2

    def Daily_Hourly_Average(df):
        uniq_hours=df.index
        df2 = df[:]
        df2['Daily_Hourly_Average']=0
        frm=uniq_hours[0]
        for i in uniq_hours[24:]:
            rng=df2[(df2.index >= frm) & (df2.index < i)]
            hr=i.hour
            avg_value=rng[rng.columns[0]][rng.index.hour==hr].mean()
            df2['Daily_Hourly_Average'][i]=avg_value
        df2['Time'] = df2.index
        return df2

