def moment_3(data_set,threshold,percentage,win_len,aggr_method,elapsed=0):    
    if(elapsed==1):
        f=open('Logs/moment_3_output_time_time_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.time()
    else:
        f=open('Logs/moment_3_output_time_processtime_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.process_time()
    print('Start Time : ' + str(datetime.datetime.now()),file=f)
    dd=data_set[:]
    #dd.index=dd.Time
    cols = [col for col in dd.columns if col not in ['Time']]
    if(elapsed==1):
        print('Time Taken For Imputation '+ str(time.time()-t0),file=f)
    else:
        print('Time Taken For Imputation '+ str(time.process_time()-t0),file=f)
    hour_data=non_overlapping_average(dd,win_len,aggr_method)
    for col in cols:
        if(elapsed==1):
            t1=time.time()
        else:
            t1=time.process_time()
        ts=dd[col]
        dff=DataFrame({'Time':dd.Time,'Actual':dd[col]})        
        tp=min(hour_data[(len(hour_data)*percentage).__round__():].index)
        Train = ts[:(tp-datetime.timedelta(minutes=1))]
        Test = ts[tp:]
        seq_len = (len(Test) // win_len)+1
        dist_metrics_df = DataFrame()
        mn3=[]
        if(col==cols[0]):
            print('Total Prediction Points:'+str(seq_len),file=f)
        avg=[]
        for i in range(seq_len):
            if(elapsed==1):
                t2=time.time()
            else:
                t2=time.process_time()
            test_data=Test[i*win_len:(i+1)*win_len]
            if(len(test_data)>0):
                moment_3 = stats.moment(test_data,moment=3)
                mn3.append(moment_3)
            if(elapsed==1):
                avg.append(time.time()-t2)
            else:
                avg.append(time.process_time()-t2)
			
        print(col+' Moment3 Prediction '+'Total Time:'+str(sum(avg))+',Average Time:'+str(st.mean(avg)),file=f)
        dist_metrics_df[col]=DataFrame(mn3,columns=[col])
        ind = pd.date_range(Test.index.min(), periods=dist_metrics_df.shape[0], freq=str(win_len) + 'min')
        dist_metrics_df.index = ind
        
        sm3=np.square(dist_metrics_df[col]).sum()
        m3=dist_metrics_df[col].mean()
        nM3=len(dist_metrics_df[col])
        
        x_square_by_n3=sm3/nM3
        #Extracting Updated Mean Value
        mean_square3=round(np.square(m3),45)
        #Calculating SD with new values
        Variance3=x_square_by_n3-mean_square3
        if(Variance3<=0):
            SD3=1e-44
        else:
            SD3=np.sqrt(Variance3)
			
        mm3=np.array(dist_metrics_df[col]-m3)/SD3
        final=DataFrame({col:mm3})
        final.index=dist_metrics_df.index
        indexes=final[col][(final[col] > threshold)].index
        final_res=final[:]
        final_res[col]=0
        final_res[col][indexes]=1
        final_res['Time']=final_res.index
        if(col==cols[0]):
            final_result=final_res
        else:
            final_result=final_result.merge(final_res,on='Time',how='outer')
        if(elapsed==1):
            print('Time Taken For: '+col+' is '+ str(time.time()-t1),file=f)
        else:
            print('Time Taken For: '+col+' is '+ str(time.process_time()-t1),file=f)
        
    if(elapsed==1):
        print('Total Time Taken: ' + str(time.time()-t0),file=f)
    else:
        print('Total Time Taken: ' + str(time.process_time()-t0),file=f)
    print('End Time : ' + str(datetime.datetime.now()),file=f)
    print('Moment3 Output DataFrame Dimension : ' + str(final_result.shape),file=f)
    
    f.close()
    final_result.index=final_result.Time
    final_result.drop('Time',axis=1,inplace=True)
    return final_result


def moment_4(data_set,threshold,percentage,win_len,aggr_method,elapsed=0):    
    if(elapsed==1):
        f=open('Logs/moment_4_output_time_time_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.time()
    else:
        f=open('Logs/moment_4_output_time_processtime_'+str(win_len)+'_Win_len and '+str(percentage)+'_Percentage'+'.log','w')
        t0=time.process_time()
    print('Start Time : ' + str(datetime.datetime.now()),file=f)
    dd=data_set[:]
    cols = [col for col in dd.columns if col not in ['Time']]
    if(elapsed==1):
        print('Time Taken For Imputation '+ str(time.time()-t0),file=f)
    else:
        print('Time Taken For Imputation '+ str(time.process_time()-t0),file=f)
    hour_data=non_overlapping_average(dd,win_len,aggr_method)
    for col in cols:
        if(elapsed==1):
            t1=time.time()
        else:
            t1=time.process_time()
        ts=dd[col]
        dff=DataFrame({'Time':dd.Time,'Actual':dd[col]})        
        tp=min(hour_data[(len(hour_data)*percentage).__round__():].index)
        Train = ts[:(tp-datetime.timedelta(minutes=1))]
        Test = ts[tp:]
        seq_len = (len(Test) // win_len)+1
        dist_metrics_df = DataFrame()
        mn4=[]
        if(col==cols[0]):
            print('Total Prediction Points:'+str(seq_len),file=f)
        avg=[]
        for i in range(seq_len):
            if(elapsed==1):
                t2=time.time()
            else:
                t2=time.process_time()
            test_data=Test[i*win_len:(i+1)*win_len]
            if(len(test_data)>0):
                moment_4 = stats.moment(test_data,moment=4)
                mn4.append(moment_4)
            if(elapsed==1):
                avg.append(time.time()-t2)
            else:
                avg.append(time.process_time()-t2)
			
        print(col+' Moment4 Prediction '+'Total Time:'+str(sum(avg))+',Average Time:'+str(st.mean(avg)),file=f)
        dist_metrics_df[col]=DataFrame(mn4,columns=[col])
        ind = pd.date_range(Test.index.min(), periods=dist_metrics_df.shape[0], freq=str(win_len) + 'min')
        dist_metrics_df.index = ind
        
        sm4=np.square(dist_metrics_df[col]).sum()
        m4=dist_metrics_df[col].mean()
        nM4=len(dist_metrics_df[col])
        
        x_square_by_n4=sm4/nM4
        #Extracting Updated Mean Value
        mean_square4=round(np.square(m4),45)
        #Calculating SD with new values
        Variance4=x_square_by_n4-mean_square4
        if(Variance4<=0):
            SD4=1e-44
        else:
            SD4=np.sqrt(Variance4)
			
        mm4=np.array(dist_metrics_df[col]-m4)/SD4
        final=DataFrame({col:mm4})
        final.index=dist_metrics_df.index
        indexes=final[col][(final[col] > threshold)].index
        final_res=final[:]
        final_res[col]=0
        final_res[col][indexes]=1
        final_res['Time']=final_res.index
        if(col==cols[0]):
            final_result=final_res
        else:
            final_result=final_result.merge(final_res,on='Time',how='outer')
        if(elapsed==1):
            print('Time Taken For: '+col+' is '+ str(time.time()-t1),file=f)
        else:
            print('Time Taken For: '+col+' is '+ str(time.process_time()-t1),file=f)
        
    if(elapsed==1):
        print('Total Time Taken: ' + str(time.time()-t0),file=f)
    else:
        print('Total Time Taken: ' + str(time.process_time()-t0),file=f)
    print('End Time : ' + str(datetime.datetime.now()),file=f)
    print('Moment4 Output DataFrame Dimension : ' + str(final_result.shape),file=f)
    f.close()
    final_result.index=final_result.Time
    final_result.drop('Time',axis=1,inplace=True)
    return final_result







#def sum_of_squares(data_set,threshold,percentage,win_len,aggr_method,elapsed=0):    
#    if(elapsed==1):
#        f=open('output_time_time_'+str(win_len)+'_Win_len.log','w')
#        t0=time.time()
#    else:
#        f=open('output_time_processtime_'+str(win_len)+'_Win_len.log','w')
#        t0=time.process_time()
#    dd=data_set[:]
#    cols = [col for col in dd.columns if col not in ['Time']]
#    ano_details=DataFrame({'KPI':cols,aggr_method+'_mnt3':0,aggr_method+'_mnt4':0,aggr_method+'_nrow':0})
#    df = non_overlapping_average(dd,1,aggr_method='mean')
#    if(elapsed==1):
#        print('Time Taken For Imputation '+ str(time.time()-t0),file=f)
#    else:
#        print('Time Taken For Imputation '+ str(time.process_time()-t0),file=f)
#    j=0
#    for col in cols:
#        if(elapsed==1):
#            t1=time.time()
#        else:
#            t1=time.process_time()
#        ts=df[col]
#        dff=DataFrame({'Time':dd.Time,'Actual':dd[col]})
#        hour_data=RD_Convert(dff,win_len,aggr_method)
#        plot_data=hour_data[(len(hour_data)*percentage).__round__():]
#        Train = ts[:(len(ts)*percentage).__round__()]
#        Test = ts[(len(ts)*percentage).__round__():]
#        dist_metrics_cols = [col + '_mnt_3', col + '_mnt_4']
#        seq_len = (len(Test) // win_len)+1
#        #dist_metrics_df = pd.DataFrame(columns=dist_metrics_cols)
#        dist_metrics_df = DataFrame({col+'_mnt_3':[np.float(1)]*seq_len,col+'_mnt_4':[np.float(1)]*seq_len})
#        if(col==cols[0]):
#            print('Total Prediction Points:'+str(seq_len),file=f)
#        avg=[]
#        for i in range(seq_len):
#            if(elapsed==1):
#                t2=time.time()
#            else:
#                t2=time.process_time()
#            moment_3 = stats.moment(Test[i*win_len:(i+1)*win_len],moment=3)
#            dist_metrics_df[dist_metrics_cols[0]][i]=moment_3
#            #moment_4 = stats.moment(Test[i*win_len:(i+1)*win_len], moment=4)            
#            #win_dist_metrics = pd.Series([moment_3,moment_4],index=dist_metrics_cols)
#            #dist_metrics_df = dist_metrics_df.append(win_dist_metrics, ignore_index=True)
#            if(elapsed==1):
#                #print(col+' Moment3 '+str(i)+' loop '+ str(time.time()-t2),file=f)
#                avg.append(time.time()-t2)
#            else:
#                #print(col+' Moment3 '+str(i)+' loop '+ str(time.process_time()-t2),file=f)
#                avg.append(time.process_time()-t2)
#        print(col+' Moment3 Prediction '+'Total Time:'+str(sum(avg))+',Average Time:'+str(st.mean(avg)),file=f)
#        avg=[]
#        for i in range(seq_len):
#            if(elapsed==1):
#                t2=time.time()
#            else:
#                t2=time.process_time()
#            moment_4 = stats.moment(Test[i*win_len:(i+1)*win_len], moment=4)
#            dist_metrics_df[dist_metrics_cols[1]][i]=moment_4
#            if(elapsed==1):
#                avg.append(time.time()-t2)
#                #print(col+' Moment4 '+str(i)+' loop '+ str(time.time()-t2),file=f)
#            else:
#                avg.append(time.process_time()-t2)
#                #print(col+' Moment4 '+str(i)+' loop '+ str(time.process_time()-t2),file=f)
#        print(col+' Moment4 Prediction '+'Total Time:'+str(sum(avg))+',Average Time:'+str(st.mean(avg)),file=f)
#        ind = pd.date_range(Test.index.min(), periods=dist_metrics_df.shape[0], freq=str(win_len) + 'min')
#        dist_metrics_df.index = ind
#        
#        sm3=np.square(dist_metrics_df[dist_metrics_cols[0]]).sum()
#        sm4=np.square(dist_metrics_df[dist_metrics_cols[1]]).sum()
#        m3=dist_metrics_df[dist_metrics_cols[0]].mean()
#        m4=np.array(dist_metrics_df[dist_metrics_cols[1]])[0].mean()
#        nM3=len(dist_metrics_df[dist_metrics_cols[0]])
#        nM4=len(dist_metrics_df[dist_metrics_cols[1]])
#        
#        x_square_by_n3=sm3/nM3
#        #Extracting Updated Mean Value
#        mean_square3=round(np.square(m3),45)
#        #Calculating SD with new values
#        Variance3=x_square_by_n3-mean_square3
#        if(Variance3<=0):
#            SD3=1e-44
#        else:
#            SD3=np.sqrt(Variance3)
#            
#        x_square_by_n4=sm4/nM4
#        #Extracting Updated Mean Value
#        mean_square4=round(np.square(m4),45)
#        #Calculating SD with new values
#        Variance4=x_square_by_n4-mean_square4
#        if(Variance4<=0):
#            SD4=1e-44
#        else:
#            SD4=np.sqrt(Variance4)
#        mm3=np.array(dist_metrics_df[dist_metrics_cols[0]]-m3)/SD3
#        mm4=np.array(dist_metrics_df[dist_metrics_cols[1]]-m4)/SD4
#        final=DataFrame({dist_metrics_cols[0]:mm3,dist_metrics_cols[1]:mm4})
#        final.index=dist_metrics_df.index
#        indexes3=final[dist_metrics_cols[0]][(final[dist_metrics_cols[0]] > threshold) | (final[dist_metrics_cols[0]] < -threshold)].index
#        indexes4=final[dist_metrics_cols[1]][(final[dist_metrics_cols[1]] > threshold) | (final[dist_metrics_cols[1]] < -threshold)].index
#        final_res=final[:]
#        final_res[dist_metrics_cols[0]]=0
#        final_res[dist_metrics_cols[0]][indexes3]=1
#        final_res[dist_metrics_cols[1]]=0
#        final_res[dist_metrics_cols[1]][indexes4]=1
#        final_res['Time']=final_res.index
#        if(col==cols[0]):
#            final_result=final_res
#        else:
#            final_result=final_result.merge(final_res,on='Time',how='outer')
#        if(elapsed==1):
#            print('Time Taken For: '+col+' is '+ str(time.time()-t1),file=f)
#        else:
#            print('Time Taken For: '+col+' is '+ str(time.process_time()-t1),file=f)
#        ano_details.loc[j,aggr_method+'_mnt3']=len(indexes3)
#        ano_details.loc[j,aggr_method+'_mnt4']=len(indexes4)
#        ano_details.loc[j,aggr_method+'_nrow']=len(hour_data)
#        j=j+1
#        
#        for k in ['_mnt3','_mnt4']:
#            plt.figure()
#            plt.plot(hour_data['Actual'])    
#            if(k=='_mnt3'):
#                plt.plot(hour_data['Actual'][indexes3],'ro')
#            else:
#                plt.plot(hour_data['Actual'][indexes4],'ro')
#            plt.axvline(x=str(plot_data.index[0]),color='r')
#            plt.title(aggr_method+k+'_'+str(col)+' With '+str(win_len)+'_Win_len')
#            plt.savefig(str(col)+'_'+aggr_method+k+' With '+str(win_len)+'_Win_len.jpg')
#    if(elapsed==1):
#        print('Total Time Taken: ' + str(time.time()-t0),file=f)
#    else:
#        print('Total Time Taken: ' + str(time.process_time()-t0),file=f)
#    print(ano_details,file=f)
#    f.close()
#    return final_result






#def AD_Regression(data_set,percentage,nhead,method,win_len,aggr_method='mean'):
#    t0=time.process_time()
#    cols = [col for col in data_set.columns if col not in ['Time']]
#    ano_details=DataFrame({'KPI':cols,aggr_method:0,aggr_method+'_nrow':0})
#    j=0
#    for col in cols: 
#        
#        t1=time.process_time()
#        df=DataFrame({'Time':data_set.Time,'Actual':data_set[col]})
#        
#        final=[]
#        
#        hour_data=RD_Convert(df,win_len,aggr_method)
#        df1=DataFrame({'Actual':hour_data.Actual})
#        for i in range(1,21):
#            df1['shift%s'%i]=df1.Actual.shift(i)
#        df2=df1.iloc[20:]
#        # Split the data into training/testing sets
#        Train = df2[:(len(df2)*percentage).__round__()]
#        Test = df2[(len(df2)*percentage).__round__():]
#        lr_cols = [col for col in df2.columns if col not in ['Actual']]
#        for i in range((len(Test) / nhead).__round__()+1):
#            if (i == 0):
#                X_Train = Train
#                X_Test = Test[0:nhead]
#                Y_Train = Train.Actual
#            else:
#                X_Train=df2[(i*nhead):(len(Train)+(i*nhead))]
#                X_Test=Test[i*nhead:(i+1)*nhead]
#                Y_Train=df2.Actual[(i*nhead):(len(Train)+(i*nhead))]            
#           
#            if(method=='Linear'):                
#                model=sm.OLS(Y_Train,X_Train[lr_cols])
#                result=model.fit()
#                Y_Pred=np.array(result.predict(X_Test[lr_cols]))
#            elif('Poly'):
#                #print('Polynomial Regression:')
#                for count, degree in enumerate([3, 4, 5]):
#                    model = make_pipeline(PolynomialFeatures(degree), Ridge())
#                    model.fit(X_Train[lr_cols], Y_Train)
#                    Y_Pred = model.predict(X_Test[lr_cols])
#            final.append(Y_Pred)
#        print('Predicted Using Linear Regression: ' + str(time.process_time()-t1))
#        result=np.concatenate([np.array(i) for i in final])
#        Actual=Test.Actual
#        error=np.array(Actual-result)
#        n=len(Actual)
#        k=20
#        sums= (np.square(error)).sum()/(n-k-1)
#        se=np.sqrt(sums)*2.5
#        output=DataFrame({'Actual':Actual,'Predicted':result,'Lower':result-se,'Upper':result+se})
#        indexes=output[(output['Actual'] < output['Lower']) | (output['Actual'] > output['Upper'])].index
#        ano_details.loc[j,aggr_method]=len(indexes)
#        ano_details.loc[j,aggr_method+'_nrow']=len(hour_data)
#        j=j+1
#        plot_data=RD_Convert(df,win_len,'mean')
#        plt.figure()
#        plt.plot(plot_data['Actual'])        
#        plt.plot(plot_data['Actual'][indexes],'ro')
#        plt.axvline(x=str(Test.index[0]),color='r')
#        plt.title('Linear Regression for '+ aggr_method+'_'+str(col))
#        plt.savefig(str(col)+'_LR_'+aggr_method+'.jpg')
#    #final_result[j]=DataFrame({j:final.reshape(-1)})
#    #print("Mean squared error: %.2f using Method:%s"%(mean_squared_error(Test.Actual, result),method))
#    #print('Variance score: %.2f using Method: %s' %(r2_score(Test.Actual, result),method))
#    #print('Total Time Taken: ' + str(time.process_time()-t0))
#    return ano_details