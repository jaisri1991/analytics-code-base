#DB variables
hostip='192.168.13.83'
comp_list='2,122,442,322'
app_id = 2
frm = '2017-02-01'
to = '2017-03-01'
txn_list='3,5,7,8,12,14,15,16,17,18,19,20,21,22,24,25,27,29,30,33,34,36,37,38,39,52,53,54,55,56,57,58,59,60,64,65,66,67,68,69,70,71,72,73,74'
nhead = 1

exec(open(Base_Dir+'/Include/Libraries.py').read())
exec(open(Base_Dir+'/Config/dbconfig.py').read())
exec(open(Source_Dir+'/Estimators.py').read())
exec(open(Source_Dir+'/Transformers.py').read())
exec(open(Models_Dir+'/helper.py').read())
exec(open(Models_Dir+'/features.py').read())
exec(open(Models_Dir+'/Linear_Regression.py').read())
exec(open(Models_Dir+'/Moments.py').read())
exec(open(Models_Dir+'/Arima.py').read())