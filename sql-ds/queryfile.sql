-- :name ENTERPRISE :? :*
-- :doc Lists all enterprise
select E.Name Name,R.TimeOffsetFromGMT Offset from apmcommon.REMOTECLIENTDEF R,apmcommon.ENTERPRISEDEF E where E.Id=R.EnterpriseId and E.Name !='A1HEALTH'

-- :name APPLICATION :? :*
-- :doc Counts the users in a given country.
select InstanceId,concat(AppName,'_',InstanceName) InstanceName from :i:ENAME .APPLICATIONDEF

-- :name MTIME :? :*
-- :doc Min and Max Time from AD_RT_ANOMALYDEF table.
select Layer,min(Time) MIN,max(Time) MAX from :i:ENAME .AD_RT_ANOMALYDEF where App_Id=:AppId group by Layer

-- :name ACOUNT_M_HOUR :? :*
-- :doc Counts of Anomaly_Scored KPI Wise hourly.
select Time,:i:DIFF ,sum(Anomaly_Scored) Anomaly from :i:ENAME .AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and Time between :FROM  and :TO and App_Id=:AppId and Layer=:Layer group by DATE(Time),HOUR(Time),:i:DIFF  order by Time

-- :name ACOUNT_M_DATE :? :*
-- :doc Counts of Anomaly_Scored KPI Wise Daily.
select DATE(Time) Time,:i:DIFF ,sum(Anomaly_Scored) Anomaly from :i:ENAME .AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and Time between :FROM  and :TO and App_Id=:AppId and Layer=:Layer  group by DATE(Time),:i:DIFF  order by Time

-- :name ACOUNT_HOUR :? :*
-- :doc Counts of Anomaly_Scored Layer Wise hourly.
select Time,:i:DIFF ,sum(Anomaly_Scored) Anomaly from :i:ENAME .AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and Time between :FROM  and :TO and App_Id=:AppId group by DATE(Time),HOUR(Time),:i:DIFF  order by Time

-- :name ACOUNT_DATE :? :*
-- :doc Counts of Anomaly_Scored Layer Wise Daily.
select DATE(Time) Time,:i:DIFF ,sum(Anomaly_Scored) Anomaly from :i:ENAME .AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and Time between :FROM  and :TO and App_Id=:AppId group by DATE(Time),:i:DIFF  order by Time

-- :name DD_CALL1 :? :*
-- :doc Returns the Metric_Name for the given period with Weightage
select s.App_Id,s.Comp_Id,s.Metric_Name,s.Layer,(s.Weightage+ifnull(j.Weight, 0)) Weightage,j.CorrelatedKPI_MetricName,j.CorrelatedKPI_Layer from (select App_Id,Comp_Id,Metric_Name,Layer,sum(Anomaly_Scored) Anomaly,Sum(Anomaly_Scored)/(select SUM(Anomaly_Scored) from AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and App_Id=:AppId And Time BETWEEN :FROM AND :TO) Weightage from AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and App_Id=:AppId And Time BETWEEN :FROM AND :TO and Layer=:Layer group by Metric_Name,Layer order by Anomaly desc) s left join AD_RT_Static_KPICorrelations j on s.Metric_Name=j.SubjectKPI_MetricName;
--select App_Id,Comp_Id,Metric_Name,Layer,sum(Anomaly_Scored) Anomaly_Scored,Sum(Anomaly_Scored)/(select SUM(Anomaly_Scored) from AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and App_Id=:i:AppId And Time BETWEEN :FROM AND :TO) Weightage from AD_RT_ANOMALYDEF where Anomaly_Scored > 0 and App_Id=:i:AppId And Time BETWEEN :FROM AND :TO and Layer=:Layer group by Metric_Name,Layer order by Anomaly_Scored desc

-- :name DD_AD :? :*
-- :doc Actual data for each kpi layer wise
call AD_RT_Fetch_Real_Data(:ENAME,:FROM,:TO,:CompId,:KpiName,:WIN,:i:AppId,:Layer);

-- :name AVAIL :? :*
-- :doc Returns Availability Alerts
select AlertId "Alert Id", AlertEventId "Alert Event Id",CompInstanceId,Name,Metric, DATE_FORMAT((Time + interval (select TimeOffsetFromGMT*1000 from apmcommon.REMOTECLIENTDEF where RemoteClientName=:ENAME) MICROSECOND),"%Y-%m-%d %H:%i:%s") Time, Description from ( select c.Name,a.Metric, a.Time, a.AlertId, a.Id AlertEventId, a.Description, c.AppInstanceId, a.CompInstanceId, al.Severity from ALERTS al,ALERTEVENTS a, view_app_comp_inst c where al.Id=a.AlertId AND al.CurrentTime = a.Time AND al.CurrentTime between :FROM and :TO AND c.AppInstanceId = :AppId AND a.Component="AVAILABILITY" AND a.CompInstanceId=c.CompInstanceId and (c.MonitorFlag=1 or c.MonitorAvailability=1) AND c.status =1 order by AppInstanceId, CompInstanceId, Metric, Time desc) b group by AppInstanceId, CompInstanceId, Metric order by Time desc;


