#!/bin/bash

MAIN_CLASS=sql_ds.core
APP_CONFIG=config.edn
LOG_CONFIG=logback.xml
JAR=sql-ds-0.1.0-standalone.jar
BIN_DIR=$PWD

# ***********************************************
# ***********************************************
OPTS="-Djava.rmi.server.hostname=10.2.0.236 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8101 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Xms256m -Xmx512m"

ARGS="${OPTS} -Dconfig=${APP_CONFIG} -Dlogback.configurationFile=${LOG_CONFIG}"

exec java $ARGS -cp ".:${JAR}" $MAIN_CLASS
